<?php
class Format{
  function num($value) {
	  if ($value > 999 && $value <= 999999) {
		$result = floor($value / 1000) . 'K';
		} elseif ($value > 999999) {
			$result = floor($value / 1000000) . 'M';
		} else {
			$result = $value;
		}
		return $result;
  }
  
  function datepost($value){
	if($value > strtotime('TODAY') * 3)
	{
		$sec = $value - strtotime('TODAY');
		if($sec < 60)
			return 'just now';
		if($sec < 3600)
			return ceil($sec / 60).' mins';
		if($sec < 86400)
			return ceil($sec / 3600).' hours';	
		if($sec < 86400 * 3)
			return ceil($sec / 86400).' days';	
		
	}
	else if($value > strtotime('1/1/2018'))
		return date('F j',$value);
	else if($value > strtotime('1/1/2017') && $value < strtotime('1/1/2018') )
		return date('F j 2017',$value);
	else
		return date('d/m/Y',$value);	
		  
  }
  function datestring($datetime, $full = false) {
    $now = new DateTime();
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
}
?>