<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Load extends MY_ApiController{
	public function __construct(){
		parent::__construct();
		$this->load->library(array('format','pagination','curl','mongo_db'));
		header('Access-Control-Allow-Origin: *');

	}
	
	public function ads()
	{
		$limit  = 30;
		$offset = $this->input->post('start') ? intval($this->input->post('start')) : 0;
		
		$where = array();
		$where_gte = array();
		$or_where = array();
		
		$sort_by   =  $this->input->post('sort') ? $this->input->post('sort') : '_id';
		
		$like_int = $this->input->post('filter_like') ? intval($this->input->post('filter_like')) : 0;
		$cmt_int = $this->input->post('filter_comment') ? intval($this->input->post('filter_comment')) : 0;
		$share_int = $this->input->post('filter_share') ? intval($this->input->post('filter_share')) : 0;
		
		if($this->user_level == 0)
		{
			$query = $this->input->post();
			if($offset > 180  || $like_int > 500 || $cmt_int > 500 || $share_int > 500 || !empty($query['list']) || !empty($query['page_id']) || !empty($query['filter_date']) || !empty($query['filter_product']) || !empty($query['filter_adtype']) || !empty($query['filter_source']) || !empty($query['niches']) || !empty($query['keyword']) || $sort_by != '_id')
			{
				exit(response(403,'<h4 class="text-center">Please <a href="javascript:;" data-toggle="modal" data-target="#pricingModal">upgrade</a> to continue!</h4>'));
			}
		}
		
		if($this->input->post('list'))
		{
			$list_id = new MongoId($this->input->post('list'));
			$list = $this->mongo_db->select(array('list_post'))->where(array('_id'=>$list_id,'user_id'=>$this->user_id))->get('fblists');
			
			if($list) {
				
				$list = current($list);
				
				$list_post = array_slice($list['list_post'],$offset,$limit);
				
				$where['_id:$in'] = $list_post;
			}
		}
		if($this->input->post('page_id'))
		{
			$where['page_id'] = $this->input->post('page_id');	
		}
		if($like_int > 0)
			$where['likes_int:$gte'] = $like_int;
		if($cmt_int > 0)	
			$where['comments_int:$gte'] =  $cmt_int;
		if($share_int > 0)
			$where['shares_int:$gte'] =  $share_int;
			
		
		if($this->input->post('keyword'))
		{
			$text = $this->input->post('keyword');
			$regex = new MongoRegex("/$text/i"); 
			if(!$this->input->post('search_by')){
				$or_where = array('page_name'=>$regex,'content'=>$regex);
			}		
			elseif($this->input->post('search_by') == 1){
				$where['page_name'] = $regex;
			}
			else{
				$where['content'] = $regex;
			}
		}
		
		
		if($this->input->post('filter_date'))
		{
			$range = explode('-', $this->input->post('filter_date'));
			if(count($range) == 2){
				$rangestart = strtotime(trim($range[0]));
				$rangeend = strtotime(trim($range[1]));
				$rangeend = $rangeend + 86399;
				$where['datepost:$gte'] = $rangestart;
				$where['datepost:$lt'] = $rangeend;
			}
		}
		if($this->input->post('filter_product'))
		{
			$where['product_id'] = intval($this->input->post('filter_product'));
		}
		if($this->input->post('filter_adtype'))
		{
			$where['fbtype'] = intval($this->input->post('filter_adtype'));
		}
		if($this->input->post('filter_source'))
		{
			$where['is_sponsored'] = intval($this->input->post('filter_source'));
		}
		if($this->input->post('niches')){
			$niches = json_decode($this->input->post('niches'),true);
			foreach($niches as $key => $row) {
				$niches[$key] = intval($row);	
			}
			$where['niches:$in'] = $niches;	
		}
		if($sort_by == 'trend') {
			//$where['datepost:$gte'] = intval(time() - 86400 * 30);	
			$sort_by = 'datepost';
			$where['likes_int:$gte'] = 1000;
		}
		
		$total = $this->mongo_db->or_where($or_where)->where($where)->where_gte($where_gte)->count('fb_posts');	
		
		$ads = $this->mongo_db->select(array('_id','comments_int','likes_int','shares_int','post_id','picture','content','datepost','page_id','page_name','picture_amz','page_picture'))->order_by(array($sort_by=>-1))->limit($limit)->offset($offset)->get('fb_posts');
		
		$count = count($ads);
		
		for($i=0;$i<$count;$i++) {
			preg_match('#([a-zA-Z0-9]+?\.[a-zA-Z]+)/[a-z0-9\-\/]+#i', $ads[$i]['content'], $result);
			$ads[$i]['link'] = 	isset($result[0]) ? '<a href="http://'.$result[0].'" target="_blank">'.$result[1].'</a>' : '';	
			$ads[$i]['datepost'] = $this->format->datepost($ads[$i]['datepost']);
		}
		
		$return = array(
			'total' => number_format($total),
			'count' => $count,
			'data' => $ads
		);
		
		echo response(200,$return);
	}
	
	public function saved()
	{	
		
		$page_favorite = $this->db->select('paf_pageid,paf_pagename')->where('paf_user',$this->user_id)->get('page_favorite')->result_array();
		
		$lists = $this->mongo_db->where('user_id',$this->user_id)->order_by(array('type'=>-1))->get('fblists');
		$categories = $this->db->select('id,name')->where(array('has_menu'=>1))->get('categories')->result_array();
		if(!isset($lists[0]['type']) || $lists[0]['type'] != 1) 
		{
			$new_list = array(
				'user_id' => $this->user_id,
				'list_name' => 'favorite',
				'type' => 1,
				'list_post' => array()
			);	
			$this->mongo_db->insert('fblists',$new_list);
			
			array_unshift($lists,$new_list);
		}
		
		
		$return =  array(
			'page_favorite' => $page_favorite,
			'lists' => $lists,
			'categories' => $categories
		);
		
		echo response(200,$return);
	}	
	
	
	public function page_add()
	{
		if(!$this->input->post('page_id')){
			exit(response(400,'error'));
		}
		
		$page_id = $this->input->post('page_id');
		
		$page = $this->mongo_db->where('page_id',$page_id)->limit(1)->get('fb_posts');
		
		if(!$page) {
			exit(response(400,'Page not found'));	
		}
		
		$check = $this->db->where(array('paf_user'=>$this->user_id,'paf_pageid'=>$page_id))->count_all_results('page_favorite');
		
		$html = '';
			
		if($check == 0)
		{
			$page = current($page);
			$new = array(
				'paf_user' => $this->user_id,
				'paf_pageid' => $page_id,
				'paf_pagename' => $page['page_name'] ? $page['page_name'] : $page_id,
			);
			$this->db->insert('page_favorite',$new);
			$html = '<li><a href="javascript:;" class="a-start" data-start="page" data-page="'.$page_id.'"><span>'.$new['paf_pagename'].'</span><i class="fa fa-times page_delete"></i></a></li>';
		}
		
		echo response(200,array('status'=>'success','html'=>$html));
			
	}
	public function page_delete()
	{
		$page_id = $this->input->post('page_id');
		if(!$page_id){
			exit();
		}
		$this->db->where(array('paf_user'=>$this->user_id,'paf_pageid'=>$page_id))->delete('page_favorite');
		echo response(200,'success');
	}
	
	public function list_add()
	{
		if(!$this->input->post('ads_id'))
		{
			exit(response(400,'Error'));
		}
		
		$ads_id  = new MongoId($this->input->post('ads_id'));

		if($this->input->post('list_id')) {
			$list_id = new MongoId($this->input->post('list_id'));
			$check = $this->mongo_db->where(array('user_id'=>$this->user_id,'_id'=>$list_id,'list_post'=>$ads_id))->get('fblists');
		}
		else {
			$check = $this->mongo_db->where(array('user_id'=>$this->user_id,'type'=>1,'list_post'=>$ads_id))->get('fblists');
		}
		if($check) {
			$check = current($check);
			$this->mongo_db->where(array('_id'=>$check['_id']))->pull('list_post',$ads_id)->update('fblists');
			echo response(200,'pull');
		}
		else{
			if(isset($list_id))
				$this->mongo_db->where(array('_id'=>$list_id))->addtoset('list_post',array($ads_id))->update('fblists');
			else
				$this->mongo_db->where(array('user_id'=>$this->user_id,'type'=>1))->addtoset('list_post',array($ads_id))->update('fblists');	
			echo response(200,'add');	
		}
	}
	
	public function list_create()
	{
		if(!trim($this->input->post('name'))){
			exit();
		}
		
		$list_name = $this->security->xss_clean($this->input->post('name'));
		
		
		$new_list = array(
			'user_id' => $this->user_id,
			'list_name' => $list_name,
			'list_post' => array()
		);
		
		$list_id = $this->mongo_db->insert('fblists',$new_list);
		
		$html = '<li class="list_add" data-list="" list-id="'.$list_id.'">'.$list_name.' <label><span></span></label></li>';
		$menu_html = '<li><a href="javascript:;" class="a-start" data-start="list" data-list="'.$list_id.'">'.$list_name.'<i class="fa fa-times list_delete"></i></a></li>';
		
		echo response(200,array('html'=>$html,'menu_html'=>$menu_html));
	}
	
	
	public function list_delete()
	{
		if(!$this->input->post('list_id')) {
			exit(response(200,'error'));
		}
		$list_id = new MongoId($this->input->post('list_id'));
		
		$this->mongo_db->where(array('user_id'=>$this->user_id,'_id'=>$list_id,'type:$ne'=>1))->delete('fblists');
		
		echo response(200,'success');
	}
	
	
}
?>