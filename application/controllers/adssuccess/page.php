<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Page extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
       
        $this->load->library(array('mongo_db'));
        
	}
	public function load()
	{
		$offset = $this->input->get('page') ? $this->input->get('page') : 0;
		$limit  = 20;
		
		$where = array();
		
		if($this->input->get('status') != '') {
			$where['fbaccs.status'] = $this->input->get('status');
		}
		
		if($this->input->get('q')) {
			$q = $this->input->get('q');	
			$where['id_page'] = $q;
		}
		else {
			$where['page_scan'] = 1;
			$where['page_error'] = 0;
		}
		
		
		$this->mongo_db->where($where);
		
		
		$total = $this->mongo_db->count('fbpages');
		//$total = 100;
		
		$pages = $this->mongo_db->order_by(array('_id'=>'asc'))->offset($offset)->limit($limit)->get('fbpages');
		
		
		
		$data = array(
			'data' => $pages
		);
		
		
		
		$pagintaion = $this->pagination($total,$limit);
		
		$html = $this->load->view('layout/table_page',$data,true);
		$html = str_replace("\r\n","",$html);
		$html = str_replace("\t","",$html);
		
		echo response(200,array('total' => number_format($total),'html'=>trim($html),'pagination'=>$pagintaion));
	}
	
	private function pagination($total,$limit)
	{
		$this->load->library('pagination');
		$config =array(
			'total_rows' => $total,
			'next_link'  => '<span><i class="fa fa-angle-double-right" ></i></span>',
			'prev_link'  => '<span><i class="fa fa-angle-double-left" ></i></span>',
			'first_link' => 'First',
			'last_link'  => 'Last',
			'num_links'  => 5,
			'per_page'   => $limit,
			'page_query_string'=>true,
			'query_string_segment'=>'page',
			
		);	
		$this->pagination->initialize($config);
		$pagination =  $this->pagination->create_links();
		$pagination =  str_replace('<a href="&amp;page=','<a href="javascript:;" data-page="',$pagination);
		//$pagination =  str_replace('">','>',$pagination);
		return $pagination;
		
	}
	
	public function hide()
	{
		$page_id = $this->input->post('page_id');
		
		if(!$page_id) {
			exit;	
		}	
		
		$page = $this->mongo_db->where('id_page',$page_id)->get('fbpages');
		
		if(!$page) {
			exit;	
		}
		
		$this->mongo_db->where('id_page',$page_id)->set('is_approved',3)->update('fbpages');
		$this->mongo_db->where('page_id',$page_id)->delete_all('fb_posts');
		
		echo response(200,'success');
	}
	
	public function denied()
	{
		$ids = $this->input->post('ids');
		
		if(!$ids || !is_array($ids)) {
			echo response(400,'error');	
			exit;
		}
		
		foreach($ids as $key => $id) 
		{
			$ids[$key] = new MongoId($id);	
		}
		
		
		$set = array(
			'admin' => $this->admin,
			'is_approved' => 3
        );
		
		$this->mongo_db->where_in('_id', $ids)->set($set)->update_all('fbpages');
		
		$this->db->set('approved_day','approved_day + '.count($ids),FALSE);
		$this->db->set('approved_month','approved_month + '.count($ids),FALSE);
		$this->db->set('approved_total','approved_total + '.count($ids),FALSE);
		$this->db->where('name',$this->admin)->update('admin');
		
		echo response(200,'Denied All Success');
	}
	
	 public function approve()
    {
		
        if (!$this->input->post('id') || !$this->input->post('product')) {
            exit;
		}
		
		$product = $this->input->post('product');
        
		$id = $this->input->post('id');
		$id = new MongoId($id);
        
		$page = $this->mongo_db->where('_id', $id)->limit(1)->get('fbpages');
		
		$page = current($page);
		
        if(!$page) {
            exit;
		}

		$set = array(
			'admin'   => $this->admin,
			'product' => intval($product),
			'country' => 1,
			'is_approved' => 2
		);
		
		$this->mongo_db->where('_id', $id)->set($set)->update('fbpages');
		
		$this->mongo_db->batch_insert('fb_posts', $page['posts']['list_post']);
			
		if($_SESSION['time_day'] != date('d')) 
		{
			$_SESSION['time_day'] = date('d');
			$this->db->set('time_day',date('d'));
			$this->db->set('approved_day',1);
		}
		else{
			$this->db->set('approved_day','approved_day + 1',FALSE);
		}
		
		if($_SESSION['time_month'] != date('m')) 
		{
			$_SESSION['time_month'] = date('m');
			$this->db->set('time_month',date('m'));
			$this->db->set('approved_month',1);
		}
		else{
			$this->db->set('approved_month','approved_month + 1',FALSE);
		}
		
		$this->db->set('approved_total','approved_total + 1',FALSE);
		
		$this->db->where('name',$this->admin)->update('admin');
		
		echo response(200,'success');
    }

    public function approve_niche(){
    	$list_niche = $this->input->post('list_niche');
    	$page_id = $this->input->post('page_id');
    	$niches = array();
    	if(empty($list_niche)){
    		echo 'Chưa chọn list';
    		exit();
    	}
    	foreach ($list_niche as $row) {
    		$niches[] = (int)$row;
    	}
    	$this->mongo_db->set(array('niches'=>$niches))->where(array('id_page'=>$page_id))->update('fbpages');
    	foreach ($niches as $row) {
    		$this->mongo_db->where('page_id', $page_id)->where_ne('niches', $row)->addtoset('niches',array($row))->update_all('fb_posts');
    	}
		
		
		
		
    	echo 1;
    }

}?>