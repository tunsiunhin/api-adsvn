<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Niche extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('mongo_db','curl'));
    }
	
	public function add()
	{
		if(!$this->input->post('niches')){
			echo response(400,'error');
			exit;
		}	

		$niches = $this->input->post('niches');
		$niches = explode("\n",$niches);
		
		$category  = $this->input->post('category') ? $this->input->post('category') : 0;
		$parent_id = $this->input->post('parent') ? $this->input->post('parent') : 0;
		
		foreach($niches as $row) {
			$row = trim($row);
			if(!$row) {
				continue;	
			}
			
			$new = array(
				'parent_id'=>$parent_id,
				'name' => strtolower($row),
				'post_count' => 0,
				'category_id'=>$category,
				'status' => 0,
			);
			
			$insert_query = $this->db->insert_string('niches', $new);
			$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
			$this->db->query($insert_query);
		}
		
		echo response(200,'success');
	}
}?>