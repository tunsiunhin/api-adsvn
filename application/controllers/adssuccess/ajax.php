<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Ajax extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
       
        $this->load->library(array('mongo_db'));
        
    }

    public function index($index)
    {
       
    }

    private function process_post()
    {
        $data = $this->input->post();
        if (!$data)
            exit();
        //$id 	= new MongoId($data['data_id']);
        if ($data['type'] == 3) {
            foreach ($data['data_id'] as $row) {
                $id = new MongoId($row);
                $this->mongo_db->where(array('_id' => $id))->set(array('approved' => 1, 'isAdmin' => $this->isAdmin))->update('fb_post_tmps');
            }
            echo $data['type'];
        } else {
            $id = new MongoId($data['data_id']);
            $row = $this->mongo_db->where(array('_id' => $id))->get('fb_post_tmps');
            $row = current($row);
            $type = 1;
            if ($data['type'] == 2)
                $type = 2;
            $check = $this->mongo_db->where(array('post_id' => $row['post_id']))->get('fb_posts');
            $check = current($check);
            if ($check) {
                $new_data = array('product_id' => $type);
                $this->mongo_db->where(array('post_id' => $check['post_id']))->set($new_data)->update('fb_posts');
            } else {
                unset($row['approved']);
                unset($row['_id']);
                $row['product_id'] = $type;
                $this->mongo_db->insert('fb_posts', $row);
            }
            $this->mongo_db->where(array('_id' => $id))->set(array('approved' => 1, 'isAdmin' => $this->isAdmin))->update('fb_post_tmps');
            echo $type;
        }
    }

    public function action_page()
    {
        if (!$this->input->post('id') || !$this->input->post('action')) {
            exit;
		}
		
        $id = $this->input->post('id');
		$id = new MongoId($id);
        
		$action = $this->input->post('action');		

		$page = $this->mongo_db->where('_id', $id)->limit(1)->get('fbpages');
		
		$page = current($page);
		
        if(!$page) {
            exit;
		}
		
        if ($action == 1) 
		{
            if(!$this->input->post('product') || !$this->input->post('post_ids')) {
                exit;
			}
			
			$product = $this->input->post('product');
			$post_ids = $this->input->post('post_ids');
			$post_ids = json_decode($post_ids,true);
			
            $set = array(
                'admin'   => $this->admin,
                'product' => intval($product),
                'country' => 1,
                'is_approved' => 2
            );
						
			
			$new_ads = array();
			
			foreach($post_ids as $row) 
			{
				$new_ads[] = array(
					'post_id' => $page['id_page'].'_'.$row,
					'page_id' => $page['id_page'],
					'type'    => $product,
					'task'    => $page['feed_ads']['fbid'],
					'source'  => 'sponsored',
					'is_sponsored' => 1
				);
			}
			
			//print_r($new_ads);
			
            $this->mongo_db->batch_insert('fb_post_ids', $new_ads);
			
            echo 'active';

        } 
		
		elseif ($action == 2) {
            $set = array(
                'admin' => $this->admin,
                'is_approved' => 3
            );
			
            echo 'hide';
        }
		
        $this->mongo_db->where('_id', $id)->set($set)->update('fbpages');
    }

    private function create_robot()
    {
        $type = $this->input->post('type');
        $post = $this->input->post();
        $task = $this->input->post('task');
        $link = $this->input->post('link');
        $time = $this->input->post('time');
        $type_id = $this->input->post('type_id');
        $country = $this->input->post('country');
        $keyword = $this->input->post('keyword');
        $source = $this->input->post('source');
        $keyword = explode("\n", $keyword);
        $time_string = '';
        $time_arr = array();
        if ($time == 1) {
            $time_arr = array
            (
                'name' => 'creation_time',
                'args' => '{"start_month":"' . date('Y-m') . '","end_month":"' . date('Y-m') . '"}'
            );
        } elseif ($time == 2) {
            $last_mon = date('m');
            $time_arr = array
            (
                'name' => 'creation_time',
                'args' => '{"start_month":"' . date('Y-m', strtotime('-1 month')) . '","end_month":"' . date('Y-m', strtotime('-1 month')) . '"}'
            );
        } elseif ($time == 3) {
            $time_arr = array
            (
                'name' => 'creation_time',
                'args' => '{"start_year":"' . date('Y') . '","end_year":"' . date('Y') . '"}'
            );
        }

        if ($time_arr)
            $time_string = rawurlencode(json_encode($time_arr));
        foreach ($keyword as $row) {

            $url = str_replace('{kw}', strtolower(urlencode(trim($row))), $link);
            $url = str_replace(' ', '+', $url);

            if ($time_string != '')
                $url .= $time_string;

            $new[] = array(
                'task' => $task,
                'link' => $url,
                'type' => $type_id,
                'next_time' => 0,
                'source' => $source
            );
        }
        if ($type == 2)
            $this->mongo_db->where(array('task' => $task))->delete_all('link_crond');
        $this->mongo_db->batch_insert('link_crond', $new);
        echo '1';
    }
}
?>