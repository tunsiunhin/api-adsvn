<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Proxy extends MY_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('curl'));
		
 	}
	
	public function check()
	{
		$proxy_id = $this->input->post('proxy_id');
		
		if(!$proxy_id) {
			exit;	
		}
		$proxy = $this->db->select('host,auth')->where('id',$proxy_id)->get('proxys')->row_array();	
		
		if(!$proxy) {
			echo response(400,'error');
			exit;	
		}
		
		$header = array(
			'accept: */*',
			'user-agent: '.$this->get_user_agent()
		);
		
		$res = $this->curl->facebook('',false,$header,$proxy);
		
		if($res['code'] == 200){
			$this->db->where('id',$proxy_id)->set('status',1)->update('proxys');	
			echo response(200,'Active');	
		}
		else{
			$this->db->where('id',$proxy_id)->set('status',0)->update('proxys');	
			echo response(200,'Error');	
		}
	}
	
	public function add()
	{
		$name = $this->input->post('name');	
		$host = $this->input->post('host');	
		$auth = $this->input->post('auth');	
		
		if(!$name || !$host) {
			echo response(400,'Error');
			exit;
		}
		
		$hosts = explode("\n", $host);
		
		$check = $this->db->where_in('host',$hosts)->get('proxys')->row_array();
		
		if($check){
			echo response(400,'Proxy existed '.$check['host']);
			exit;	
		}
		
		
		foreach($hosts as $key => $row) 
		{
			if(!$row) {
				continue;	
			}
			$split_host = explode(':',$row);
			
			if(count($split_host) < 2) 
			{
				echo response(400,$row.' Error');
				die;	
			}
						
			$proxys[] = array(
				'name' => str_replace('{key}', substr($split_host[0],-3,3), $name),
				'host' => $row,
				'auth' => $auth,
				'created' => time(),
				'status' => 1
			);
			
		}
		
		$this->db->insert_batch('proxys',$proxys);
		
		echo response(200,'Success');

	}
	
	private function get_user_agent()
	{
		$user_agents = array(
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
			'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36'
		);		
		
		return $user_agents[array_rand($user_agents)];
	}
}?>