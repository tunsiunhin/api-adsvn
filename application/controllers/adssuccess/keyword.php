<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Keyword extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
       
        $this->load->library(array('mongo_db'));
        
	}
	public function index()
	{
		$keyword 	= $this->input->post('keyword');
		$category 	= $this->input->post('category_id');
		$link 		= $this->input->post('link');
		$type 	= $this->input->post('type');
		if(!$keyword || !$category)
		{
			echo 'error';
			exit;		
		}
		if($type == 'add_new')
		{
			$this->db->where(array('category'=>$category_id))->delete('link_crond');
			$this->db->where(array('category_id'=>$category_id))->delete('keywords');
			$this->db->insert('keywords',array('keyword'=>$keyword,'category_id'=>$category_id));	
		}else
		{
			$old_key = $this->db->where(array('category_id'=>$category_id))->get('keywords')->rows_array();
			if($old_key)
				$update = $old_key['keyword']+'\n'+$keyword;
			else
				$update = $keyword;
			$this->db->where(array('category_id'=>$category_id))->set(array('keyword'=>$update))->update('keywords');
		}
		$keyword = explode("\n",$keyword);	
		foreach($keyword as $row)
		{
			$row = preg_replace('/ +/','+',$row);
			$link_crond = str_replace('{kw}',$row,$link);
			$news[] = array(
				'link' =>$link_crond,
				"category" =>$category,
				"next_update" => time(),
				"source" => 1
			);	
			
		}
		$this->db->insert_batch('link_crond',$news);
		echo 'success';
	}
}?>