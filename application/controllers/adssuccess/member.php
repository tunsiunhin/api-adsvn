<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		session_start();
	}
	public function login()
	{
		$this->load->view('login');
	}
	
	public function process_login()
	{
		$username = $this->input->post('userName');
		$password =	$this->input->post('password');
		$admin = array(
			'admin_vip' => array(
				'level' => 1,
				'password' => 'thienglieng'
			)
		);
		
		if(!isset($admin[$username]))
			exit();
		
		if(strcmp($password,$admin[$username]['password']) != 0)
		{
			exit;
		}
		
		$_SESSION['time_day']   = 1;
		$_SESSION['time_month'] = 1;
		$_SESSION['admin']      = $username;
		$_SESSION['level']      = $admin[$username]['level'];
			
		exit('1');
	}
	
	public function logout()
	{
		$helper = array_keys($_SESSION);
		foreach ($helper as $key){
			unset($_SESSION[$key]);
		}
		redirect(base_url().'login');	
	}
}
?>