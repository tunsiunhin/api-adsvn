<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class View extends MY_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('mongo_db','curl'));
		
 	}
	public function index()
    {
		
    }
	public function dashboard()
	{
		
		$this->view('dashboard');
	}
	
	public function member()
	{
		$offset = $this->input->get('page') ? $this->input->get('page') : 0;
		
		$this->data['data'] = $this->db->offset($offset)->limit(30)->get('user')->result_array();
		$this->view('member_view');
	}
	
	public function approve_page()
	{
		
		$admin = $this->db->where('name',$this->admin)->get('admin')->row_array();
		
		$adkey = array(
			'admin_v1' => 0,
			'admin_v2' => 1,
			'admin_v3' => 2
		);
		
		$offset = isset($adkey[$this->admin]) ? $adkey[$this->admin] * 50 : 0;
				
		$data = $this->mongo_db->select(array('_id','id_page','page_url','page_picture','page_name','page_like','posts'))->where(array('is_approved'=>1,'posts.status'=>1))->order_by(array('page_like'=>'asc'))->offset($offset)->limit(50)->get('fbpages');
		
		$this->data['pages'] = $data;
		$this->data['admin'] = $admin;
		$this->view('approve_page_view');
	}
	public function approve_post()
	{
		
		 // echo response(400,'Tạm dừng duyệt post');
		 // 	exit;
		
		$start = 0;
		
		$this->mongo_db->where(array('approved'=>0))->offset($start)->where_ne('source',3)->order_by(array('datepost'=>'ASC'))->limit(200);
				
		$this->data['count']    = $this->mongo_db->count('fb_post_tmps');
		$this->data['fbposts']  = $this->mongo_db->get('fb_post_tmps');
		
		$this->data['niches'] = $this->db->get('niches')->result_array();
		$this->view('approve_post_view');
	}
	public function proxy()
	{
		if($this->level != 1) {
			echo response(400,'Unauthorised');
			exit;	
		}
		$this->data['data'] = $this->db->get('proxys')->result_array();
		$this->view('proxy_view');	
	}
	
	public function account()
	{	
	
		if($this->level != 1)
		{
			echo response(400,'Unauthorised');
			exit;	
		}
				
		$this->data['proxys'] = $this->db->where(array('status'=>1))->get('proxys')->result_array();
		
		$this->data['total_account'] = $this->db->count_all_results('fbaccs');
		$this->data['total_error'] = $this->db->where('status',0)->count_all_results('fbaccs');
		$this->data['total_checkpoint'] = $this->db->where('status',4)->count_all_results('fbaccs');
		$this->data['total_limit'] = $this->db->where('status',5)->count_all_results('fbaccs');
		
		$this->view('account_view');	
	}
	
	public function page()
	{
		if($this->level != 1) {
			echo response(400,'Unauthorised');
			exit;	
		}
		$this->data['total']       =  $this->mongo_db->count('fbpages',true);
		$this->data['total_error'] =  $this->mongo_db->where('page_error',1)->count('fbpages',true);
		$this->data['total_approved'] = $this->mongo_db->where('is_approved',2)->count('fbpages',true);
		$this->data['total_denined'] = $this->mongo_db->where('is_approved',3)->count('fbpages',true);
		$this->data['total_need_approved'] = $this->mongo_db->where('is_approved',1)->count('fbpages',true);
		$this->data['total_scan'] = $this->mongo_db->where_gt('scan_count',0)->count('fbpages');
		
		
		$this->view('page_view');	
	}
	
	public function post()
	{
		
		$this->view('keyword_view');
	}
	
	public function keyword()
	{
		if($this->level != 1)
		{
			echo response(400,'Unauthorised');
			exit;	
		}
		$this->data['categories'] = $this->db->get('categories')->result_array();
		$this->view('keyword_view');	
	}
	
	public function niche()
	{
		if($this->level != 1) {
			echo response(400,'Unauthorised');
			exit;	
		}
		$this->data['niches'] = $this->db->order_by('parent_id','asc')->get('niches')->result_array();
		
		$this->view('niche_view');	
	}

	public function page_niche(){	
		$niches = $this->db->get('niches')->result_array();	
		$data = $this->mongo_db->where(array('is_approved'=>2, 'niches'=>NULL))->limit(30)->get('fbpages');
		$page = array_column($data, 'id_page');
		$posts = $this->mongo_db->where('is_sponsored', 1)->where_in('page_id', $page)->get('fb_posts');
		foreach ($data as $k => $p) {
			$c = 0;
			$data[$k]['list_post'] = array();
			foreach ($posts as $key => $value) {
				if($value['page_id'] == $p['id_page']){
					$data[$k]['list_post'][] = $value;
					$c++;
					if($c == 3){
						break;
					}
				}
			}				
		}

		$this->data['pages'] = $data;
		$this->data['niches'] = $niches;
		$this->data['posts'] = $posts;
		$this->view('page_niche');
	}


	public function categories()
	{
	
		$this->data['categories'] = $this->db->get('categories')->result_array();
		$this->view('categories');
	}
}?>