<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Post extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
       
        $this->load->library(array('mongo_db'));
        
    }
	
	public function approve()
    {
        $id = $this->input->post('id');
		$page_id = $this->input->post('page_id');
            
		$id = new MongoId($id);
		
		$row = current($this->mongo_db->where(array('_id' => $id))->get('fb_post_tmps'));
		
		if(!$row) {
			echo response(400,'error');
			exit;	
		}
		
		$check = $this->mongo_db->where(array('post_id' => $row['post_id']))->get('fb_posts');
		
		if(!$check) 
		{
			
			unset($row['approved']);
			unset($row['_id']);
		   
			$row['product_id'] = 1;
			$row['niches'] = array();
			
			$this->mongo_db->insert('fb_posts', $row);
			
			$page[] = array(
				'id_page' => current(explode('_',$row['post_id'])),
				'page_name' => '',
				'page_picture' => '',
				'page_like' => 0,
				'page_scan' => 0,
				'page_crawlcount' => 0,
				'page_error' => 0,
				'error_code' => 0,
				'time_ads' => 0,
				'product' => 0,
				'page_of' => 'bot',
				'is_approved' => 0,
				
			);
			
			$this->mongo_db->batch_insert('fbpages',$page);
		
			$this->mongo_db->where(array('_id' => $id))->set(array('approved' => 1, 'isAdmin' => $this->admin))->update('fb_post_tmps');
			
			echo response(200,'success');
        }
    }
	
	function tmp()
	{
		$posts = $this->mongo_db->where('page_id',NULL)->get('fb_posts');
		foreach($posts as $row) {
			
			$news[] = array(
				'_id' => $row['_id'],
				'page_id' => current(explode('_',$row['post_id']))
			);	
		}
		if(isset($news))
			$this->mongo_db->batch_update('fb_posts',$news);	
		
	}
	
	public function denined()
	{
		$ids = $this->input->post('ids');
		
		$new_ids = array();
		
		foreach($ids as $row) 
		{
			$new_ids[] = array(
				'_id' => new MongoId($row),
				'approved' => 1,
				'isAdmin' =>  $this->admin
			);			
		}	
		
		if($new_ids) {
			$this->mongo_db->batch_update('fb_post_tmps',$new_ids);	
		}
		
		echo response(200,'success');
	}
}?>