<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Main extends MY_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('mongo_db');
		
 	}
	public function index()
    {
		
    }
	public function dashboard()
	{
		$this->view('dashboard');
	}
	public function page()
	{
		$data = $this->mongo_db->where(array('is_approved'=>1))->limit(100)->get('fbpages');
		$this->data['pages'] = $data;
		$this->view('page');
	}
	public function robot()
	{
	    $t = '';
        $res  = $this->getData->getAnalytics();
        $data = array();
        $task = array('cron1','cron2','cron3','cron4','cron8','cron11','cron13');
        if(isset($res['groups']['result']))
        {
            foreach($res['groups']['result'] as $k =>  $v)
            {
                if(in_array($v['_id'],$task))
                    $data[] = $v;
            }
        }
        $this->data['analytics'] = $data;
		$this->data['robot'] = $this->getData->getRobot($t);
		$this->view('robot');
	}
	public function load_link()
    {
        $task = $this->input->post('task') ? $this->input->post('task') : '';
        $data = $this->getData->getRobot($task);
        echo json_encode($data);
    }
    public function del_link()
    {
	    $id = $this->input->post('id');
	    $id = new MongoId($id);
	    $c = $this->mongo_db->where('_id', $id)->delete('link_crond');
	    echo json_encode($c);
    }
	public function analytic()
	{
		$res  = $this->getData->getAnalytics();
		$data = array();
		$task = array('cron1','cron2','cron3','cron4','cron8','cron11','cron13');
		if(isset($res['groups']['result']))
		{
			foreach($res['groups']['result'] as $k =>  $v)
			{
				if(in_array($v['_id'],$task))
					$data[] = $v;
			}
		}
		$this->data['analytics'] = $data;
		$this->view('analytics');
	}
	public function fbposts()
	{
		$this->load->model('main_model','getData');
		$res = $this->getData->getFbposts($this->admin);
		$this->data['fbposts']  = $res['posts'];
		$this->data['count']  = $res['count'];
		$this->view('fbposts');
	}

    public function fbposts_page()
    {
        $res = $this->getData->getFbpostsPage();
        $this->data['fbposts']  = $res['posts'];
        $this->data['count']  = $res['count'];
        $this->view('fbposts');
    }
	public function analytics_pages()
	{
		$this->data['analytics'] = $this->getData->getAnaPage();
		$this->data['analytics_per_day'] = $this->getData->getAnaPagePerDay();
		$this->view('analytics_page');
	}
    
}
?>