<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Browser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('mongo_db'));
    }
    public function index()
    {
        $time = time() - 172800;
        $posts = $this->mongo_db->where('approved', 0)->where_ne('source', 3)->where_ne('browse', 1)->where_lte('datepost', $time)->limit(150)->order_by(array('datepost'=>'ASC'))->get('fb_post_tmps');
        foreach ($posts as $post) {
            $id = new MongoId($post['_id']);
            if ($post['likes_int'] < 100 && $post['comments_int'] < 5) {
                $this->mongo_db->where('_id', $id)->set(array('approved' => 1, 'browse' => 1))->update('fb_post_tmps');
            } else {
                $this->mongo_db->where('_id', $id)->set(array('browse' => 1))->update('fb_post_tmps');
            }
        }
    }
}