<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Check extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('mongo_db'));
    }
    public function index()
    {
        $cron = array(
            '1' => 'cron1',
            '2' => 'cron2',
            '3' => 'cron3',
            '4' => 'cron4',
            '5' => 'cron5',
            '6' => 'cron6',
            '7' => 'cron7',
            '8' => 'cron8',
            '9' => 'cron9',
            '10' => 'cron10',
            '11' => 'cron11',
            '12' => 'cron12',
            '14' => 'cron13'
        );

        foreach ($cron as $task) {
            $this->mongo_db->or_where(array('task' => $task));
        }
        $total_post_id = $this->mongo_db->count('fb_post_ids', true);
        $total_id = current($this->mongo_db->select(array('total_post_id'))->order_by(array('_id' => 'desc'))->get('analytic_post'));
        if ($total_id)
            $total_id = $total_id['total_post_id'];
        else
            $total_id = 0;
        $result_post_id = $total_post_id - $total_id;

        foreach ($cron as $task) {
            $this->mongo_db->or_where(array('task' => $task));
        }
        $total_post = $this->mongo_db->count('fb_posts', true);
        $total = current($this->mongo_db->select(array('total_post'))->order_by(array('_id' => 'desc'))->get('analytic_post'));
        if ($total)
            $total = $total['total_post'];
        else
            $total = 0;
        $result_post = $total_post - $total;
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $time_start = strtotime('TODAY');
        $time_end = $time_start + 86399;
        $count_today = $this->mongo_db->where(array('status' => 1))->where_gte('datepost', $time_start)->where_lte('datepost', $time_end)->count('fb_posts');
        $arr = array(
            'total_post_id' => $total_post_id,
            'result_post_id' => $result_post_id,
            'total_post' => $total_post,
            'result_post' => $result_post,
            'count_today' => $count_today,
            'time' => time()
        );
        $this->mongo_db->insert('analytic_post', $arr);
    }
}