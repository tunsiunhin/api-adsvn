<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Fbaccount extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('curl','mongo_db'));
		
 	}
	
	public function load()
	{
		$offset = $this->input->get('page') ? $this->input->get('page') : 0;
		$limit  = 20;
		
		$where = array();
		$q = '';
		
		if($this->input->get('status') != '') {
			$where['fbaccs.status'] = $this->input->get('status');
		}
		
		if($this->input->get('q')) {
			$where['fbaccs.fbid'] = $this->input->get('q');	
		}
		
		
		$this->db->select('fbaccs.id,fbaccs.fbid,fbaccs.password,fbaccs.cookie,fbaccs.proxy_id,fbaccs.user_agent,fbaccs.page_count,fbaccs.scan_type,fbaccs.next_run,fbaccs.added_date,fbaccs.stop,fbaccs.status,proxys.host,proxys.name as proxy_name,proxys.status as proxy_status');
		$this->db->from('fbaccs');
		$this->db->join('proxys','proxys.id=fbaccs.proxy_id');
		$this->db->where($where);
		
		
		
		$accounts = $this->db->order_by('proxys.id','asc')->offset($offset)->limit($limit)->get()->result_array();
		
		$total = $this->db->where($where)->count_all_results('fbaccs');
		
		$data = array(
			'data' => $accounts
		);
		
		$pagintaion = $this->pagination($total,$limit);
		
		$html = $this->load->view('layout/table_account',$data,true);
		$html = str_replace("\r\n","",$html);
		$html = str_replace("\t","",$html);
		
		echo response(200,array('html'=>trim($html),'pagination'=>$pagintaion));
	}
	
	private function pagination($total,$limit)
	{
		$this->load->library('pagination');
		$config =array(
			'total_rows' => $total,
			'next_link'  => '<span><i class="fa fa-angle-double-right" ></i></span>',
			'prev_link'  => '<span><i class="fa fa-angle-double-left" ></i></span>',
			'first_link' => 'First',
			'last_link'  => 'Last',
			'num_links'  => 5,
			'per_page'   => $limit,
			'page_query_string'=>true,
			'query_string_segment'=>'page',
			
		);	
		$this->pagination->initialize($config);
		$pagination =  $this->pagination->create_links();
		$pagination =  str_replace('<a href="&amp;page=','<a href="javascript:;" data-page="',$pagination);
		//$pagination =  str_replace('">','>',$pagination);
		return $pagination;
		
	}
	
	public function pause()
	{
		$acc_id = $this->input->post('acc_id');
		
		if(!$acc_id) {
			exit;	
		}
		
		$acc = $this->db->where('id',$acc_id)->get('fbaccs')->row_array();
		
		if(!$acc) {
			echo response(400,'Account not found');
			exit;	
		}
		
		$acc['stop'] = ($acc['stop'] == 1) ? 0 : 1;
		
		$this->db->where('id',$acc_id)->set('stop',$acc['stop'])->update('fbaccs');
		
		echo response(200,$acc['stop']);
		
	}
	
	public function update()
	{
		$acc_id = $this->input->post('acc_id');
		$cookie = $this->input->post('cookie');
		$password = $this->input->post('password');
		
		if(!$acc_id || !$cookie) {
			exit;	
		}
		
		$cookies = json_decode($cookie,true);
		
		if(json_last_error() !== 0) {
			echo response(400,'Cookie error');
			exit;	
		}
		
		$acc = $this->db->where('id',$acc_id)->get('fbaccs')->row_array();
		
		if(!$acc) {
			echo response(400,'Account not found');
			exit;	
		}
		
		$new_cookie = array(
			
		);
		
		foreach($cookies as $row) {
			if(isset($row['name']) && isset($row['value']) && in_array($row['name'],array('c_user','xs','spin')) !== false) {
				$new_cookie[$row['name']] = $row['value'];
			}		
		}
		
		if(count($new_cookie) < 2) {
			echo response(400,'Cookie invalid');
			exit;
		} 
		
		$set = array(
			'cookie' =>  json_encode($new_cookie)
		);
		
		if($password) {
			$set['password'] = $password;
		}
		
		$this->db->where('id',$acc_id)->set($set)->update('fbaccs');
		
		echo response(200,'Success');
		
	}
	
	public function check()
	{
		$acc_id = $this->input->post('acc_id');
		
		if(!$acc_id) {
			exit;	
		}
		
		$this->db->select('fbaccs.id,fbaccs.fbid,fbaccs.cookie,fbaccs.proxy_id,fbaccs.user_agent,fbaccs.status,proxys.host,proxys.auth');
		$acc = $this->db->from('fbaccs')->join('proxys','proxys.id=fbaccs.proxy_id')->where('fbaccs.id',$acc_id)->get()->row_array();
		
		if(!$acc) {
			echo response(400,'Account not found');
			exit;	
		}
		
		$proxy = array(
			'host' => $acc['host'],
			'auth' => $acc['auth']
		);
		
		$cookies = json_decode($acc['cookie'],true);
				
		$header = array(
			//'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			//'upgrade-insecure-requests: 1',
			'cookie: '.$this->cookie_string($cookies),
			'user-agent: '.$acc['user_agent']
		);
		
		$res = $this->curl->facebook('',false,$header,$proxy,true);
		
		if($res['code'] != 200 || !$res['response']) {
			echo response(400,'proxy error');
			exit;	
		}
		
		if(preg_match('/URL=\/checkpoint\/block/',$res['response']))
		{
			echo response(200,'Checkpoint');
			exit;
		}
		
		if($res['header']) 
		{
			$new_cookies =  $this->get_cookie($res['header']);
		
			foreach($new_cookies as $key => $row) {
				$cookies[$key] = $row;	
			}	
			
			$this->db->where('id',$acc_id)->set('cookie',json_encode($cookies))->update('fbaccs');
			
		}
		
		
		
		if(strpos($res['response'],$acc['fbid']) !== false)
		{
			preg_match('/lang="(.*?)"/',$res['response'],$match_lang);
			
			$lang = isset($match_lang[1]) ? $match_lang[1] : 'N/a';
			
			echo response(200,$acc['fbid'] .' - '.$lang);
			
			if($acc['status'] == 0 || $acc['status'] == 4) {
				$this->db->where('id',$acc_id)->set('status',1)->update('fbaccs');	
			}
		}
		else {
			echo response(200,'Die');	
		}
		
		
	
	}
	
	public function language()
	{
		$acc_id = $this->input->post('acc_id');

		if(!$acc_id) {
			exit;	
		}
		
		$acc = $this->db->from('fbaccs')->join('proxys','proxys.id=fbaccs.proxy_id')->where('fbaccs.id',$acc_id)->get()->row_array();
		
		if(!$acc) {
			echo response(400,'Account not found');
			exit;	
		}
		
		$proxy = array(
			'host' => $acc['host'],
			'auth' => $acc['auth']
		);
		
		$cookies = json_decode($acc['cookie'],true);
						
		$header = array(
			'accept: */*',
			'content-type: application/x-www-form-urlencoded',
			'origin: https://www.facebook.com',
			'referer: https://www.facebook.com/',
			'cookie: '.$this->cookie_string($cookies),
			'user-agent: '.$acc['user_agent']
		);
		
		
		
		$res = $this->curl->facebook('',false,$header,$proxy);
		
		if($res['code'] != 200 || !$res['response']) {
			echo response(400,'Proxy Error');
			exit;	
		}
		
		preg_match('/name="fb_dtsg" value="(.*?)"/',$res['response'],$fbdtsg);
		
		if(!isset($fbdtsg[1])) {
			echo response(400,'Not found fbdtsg');
			exit;
		}
		
		$param = array(
			'loc' => 'en_US',
			'ref' => 'www_card_selector',
			'__user' => $acc['fbid'],
			'fb_dtsg' => $fbdtsg[1],
			'__pc'=>'PHASED%3ADEFAULT'
		);
		
		$res = $this->curl->facebook('intl/ajax/save_locale/?dpr=1',$param,$header,$proxy,true);
		print_r($res);die;
		
		if(isset($cookies['spin'])) 
		{
			$spins = explode('_',$cookies['spin']);
			
			foreach($spins as $spin)
			{
				$spin = explode('.',$spin);
				if(count($spin) > 1) {
					$param['__spin_'.$spin[0]] = $spin[1];
				}
			}
		}
		
		
		$res = $this->curl->facebook('intl/ajax/save_locale/?dpr=1',$param,$header,$proxy,true);
		
		if($res['code'] != 200 || !$res['header']){
			echo response(400,'Save locale error');
			exit;	
		}
		
		$new_cookies =  $this->get_cookie($res['header']);
		
		foreach($new_cookies as $key => $row) {
			$cookies[$key] = $row;	
		}
		
		$this->db->where('id',$acc_id)->set('cookie',json_encode($cookies))->update('fbaccs');
		
		echo response(200,'success');
	}
	
	public function add()
    {
		$account = $this->input->post('account');
		$proxy_id = $this->input->post('proxy_id');
		
		if(!$account || !$proxy_id) {
			echo response(400,'error');
			exit;	
		}
		
		$proxy = $this->db->select('host,auth')->where('id',$proxy_id)->get('proxys')->row_array();
		
		if(!$proxy) {
			echo response(400,'Proxy not found');
			exit;	
		}
		
		$accounts = explode("\n",$account);
		

		$error = "";
		
		foreach($accounts as $key => $row) 
		{
			$acc    = explode("|",$row);
			
			if(count($acc) < 4)
			{
				continue;	
			}
			
			$fbid   = $acc[0];
			$pass   = $acc[1];
			$cookie = $acc[2];
			$token  = $acc[3];
			
			$cookies = base64_decode($cookie);
			$cookies = explode('|',$cookies);
			$cookies = array(
				'c_user' => $cookies[0],
				'xs'     => $cookies[1]
			);
			
			$next_run = $key * 60;
			$next_run += time();
			
			$new_acc = array(
				'fbid'    => trim($fbid),
				'proxy_id' => $proxy_id,
				'password' => $pass,
				'cookie'   => json_encode($cookies),
				'token'    => $token,
				'user_agent'  => $this->get_user_agent(),
				'origin_data' => $row,
				'added_date'  => time(),
				'next_run' => $next_run,
				'status'   => 1
			);
			
			$insert_query = $this->db->insert_string('fbaccs', $new_acc);
			$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
			$this->db->query($insert_query);	
		}
		
		echo response(200,'success');
		
    }
	
	private function login($proxy,$cookies)
	{
		
		$user_agent = $this->get_user_agent();
		
		$header = array(
			'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'upgrade-insecure-requests: 1',
			'cookie: '.$this->cookie_string($cookies),
			'user-agent: '.$user_agent
		);
		
		$res = $this->curl->facebook('',false,$header,$proxy,true);
		
		if($res['code'] != 200) {//Proxy ERror
			return array(
				'error' => 1
			);	
		}
		if(preg_match('/URL=\/checkpoint\/block/',$res['response'])){ // Checkpoint
			return array(
				'error' => 2
			);
		}
		
		preg_match('/"USER_ID":"([0-9]+?)"/',$res['response'],$user_id);
		
		if(count($user_id) < 2 || $user_id[1] == 0) {
			return array(
				'error' => 3
			);	
		}
		
		
		$new_cookies =  $this->get_cookie($res['header']);
		
		foreach($new_cookies as $key => $row) {
			$cookies[$key] = $row;	
		}
		
		return array(
			'cookies' => $cookies,
			'user_agent' => $user_agent
		);
	}
	
	private function get_user_agent()
	{
		$user_agents = array(
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
			'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36'
		);		
		
		return $user_agents[array_rand($user_agents)];
	}
	
	private function get_cookie($header)
	{
		preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
		
		$cookies = array();
		
		foreach($matches[1] as $item) {
			parse_str($item, $cookie);
			if( !trim(str_replace('"','',current($cookie))) ) {
				continue;	
			}
			$cookies = array_merge($cookies, $cookie);
		}
		
		return $cookies;
	}
	
	private function cookie_string($cookies){
		$res = '';
		foreach($cookies as $key => $cookie) {
			$res .= $key.'='.$cookie.';';	
		}	
		return $res;
	}
}?>