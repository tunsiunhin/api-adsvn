<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Analytics extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('mongo_db','adssuccess');
 	}
	public function save_ana_page()
	{
		$date = strtotime('TODAY');
	
		$res['page_active_crawl'] = $this->db->where(array('page_status'=>1,'feed !='=>'','page_crawlcount > '=>1))->count_all_results('fbpages') +  $this->db->where(array('page_status'=>1,'feed !='=>'','page_crawlcount'=>0))->count_all_results('fbpages');
		$res['crawl_id_from_page'] = $this->mongo_db->where(array('task'=>'page1'))->count('fb_post_ids');
		$res['crawl_post_from_page'] = $this->mongo_db->where(array('task'=>'page1'))->count('fb_post_tmps');
		$res['posts_active_auto_accpect'] = $this->mongo_db->where(array('task'=>'page1'))->count('fb_posts');
		$insert = $res;
		$insert['time_update'] = $date;
		$insert['task'] = 'page1';
		$this->mongo_db->insert('analytics',$insert);

	}
	
}
?>