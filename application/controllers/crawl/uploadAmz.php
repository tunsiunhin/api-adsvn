<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class UploadAmz extends CI_Controller{
	// upload Picture picture_amz = 0 ;=>1
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('Mongo_db','curl'));
	
 	}
	public function execute()
	{
		$params = $this->input->get();
		$params['offset'] = 0;
		$params['token'] = 'EAAAAAYsX7TsBALnpPVPamaXJ1PXac2iyyUtZBGgx6N5O9TIa7oU1bbnnk6tZAJngsqm1rBXW3RpKFTuUGjhCS2aZB9HGzOYiHWRPlZAy6ZBsveVVHXVaQ7Kvs5a4D5ZAo6TAXr1CsFO5KhOyoANZCOZAdyF7IsAqcOG5ABlDmZBT5FgZDZD';
		if(!$params)
			exit();
			
		$limit = 10;
		$offset = $params['offset'] * $limit;
		//$id 	= $params['id'];
		$token 	= $params['token'];
		$news = array();
		$posts = $this->mongo_db->select(array('post_id','page_id'))->where(array('picture_amz'=>0,'status'=>1))->limit($limit)->offset($offset)->order_by(array('_id'=>'asc'))->get('fb_posts');
		
		foreach($posts as $row)
		{
			$path = $row['post_id'].'?fields=full_picture,from{picture}&access_token='.$token;
			
			$res = $this->curl->fb_call($path);
			
			if(isset($res['error']))
			{
				if($res['error']['code'] == 190)
				{
					$this->db->where(array('id'=>$id))->update('fbaccs',array('token_error'=>1));
					continue;
				}
				$news[] = array(
					'_id' => $row['_id'],
					'picture_amz' => 0
				);
				continue;
			}
			
			$img      = isset($res['full_picture']) ? $res['full_picture'] : '';
			$page_picture = '';
			if(isset($res['from']['picture']['data']['url']))
				$page_picture = $res['from']['picture']['data']['url'];
			if(isset($res['from']['picture']))
				$page_picture = $res['from']['picture'];
			if(!empty($img))
			{
				$post_amz = $this->curl->amz_upload($img,$row['post_id'],'posts','picture/post'.$offset.'.jpg');
				$page_amz = $this->curl->amz_upload($page_picture,$row['page_id'],'pages','picture/page'.$offset.'.jpg');
			} 
			
			
			$news[] = array
			(
				 '_id'		=> $row['_id'],
				 'picture'	=> $img,
				 'page_picture' => $page_picture,
				 'picture_amz'=>isset($post_amz['success']) ? $post_amz['success'] : 4
			);
			
			sleep(2);	
					
		}
		if($news)
			$this->mongo_db->batch_update('fb_posts',$news);
	}

}
?>