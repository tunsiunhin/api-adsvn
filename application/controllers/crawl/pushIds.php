<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class PushIds extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('Mongo_db','curl'));
	
 	}
	public function index()
	{	
		$task  = $this->input->post('task');	
		$data  = $this->input->post('data');
		$source = $this->input->post('source');
		$category = $this->input->post('category');
		
		if(!$category || !$task || ! $data)
		{
			echo 'error  params';
			exit;
		}
		
		$data = json_decode($data,true);
		
		$pageids = array();
		
		foreach($data as $key => $row)
		{
			$data[$key]['category']   = $category;
			$data[$key]['task']   = $task;
			$data[$key]['error']  = 0;
			$data[$key]['status'] = 0;
			$data[$key]['source'] = $source;
			
			if(!isset($row['page_id']))
				continue;
				
			if(in_array($row['page_id'],$pageids) === false)
			{
				
				$pageids[] = $row['page_id'];
				
				$new_page[] = array(
					'id_page' => $row['page_id'],
					'feed'    => '',
					'page_status' => 0
				);
			}
		}
	
		if(isset($new_page))
		{
			//$this->mongo_db->batch_insert('fb_pages', $new_page);	
		}
		
		$this->mongo_db->batch_insert('fb_post_ids', $data);
		echo 'success';
	}
	public function getUrl()
	{
		$data = $this->db->order_by('next_update','asc')->limit(1)->get('link_crond')->row_array();
		if($data)
		{
			echo $data['link'].'?category='.$data['category'].'&task=extension&source=1';
			$this->db->where(array('id'=>$data['id']))->set(array('next_update'=>time()))->update('link_crond');
		}
	}
}
?>