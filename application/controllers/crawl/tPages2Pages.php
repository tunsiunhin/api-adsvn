<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class TPages2Pages extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('mongo_db','curl'));
	
 	}
	public function index()
	{
		$time  = time();
		$tokens = $this->getToken();
		$pages = $this->mongo_db->where(array('status'=>0))->limit(20)->get('fb_page_tmps');
		$results = array(); 
		$update = array(); // update back to post ids
		if(!$tokens || !$pages)
			exit;
		
		foreach($pages as $page)
		{
			$queries[] = array(
				'method'=>'GET',
				'relative_url' =>$page['page_id'].'?fields=fan_count,picture,feed{message,created_time,full_picture,picture},name'
			);
		}
		
		foreach($tokens as $token)
		{
			$res = $this->curl->fb_call('?batch='.json_encode($queries).'&method=post&access_token='.$token);
			
			if(isset($res['error']))
			{
				// cap nhat lai token
				continue;
			}	
			else
			{
				$results = $res;
				break;
			}
				
		}

		if(!$results)
			exit();
		
		foreach($results as $key => $row)
		{
			$info = json_decode($row['body'],true);

			if($row['code'] == 400)
			{
				$is_user = 0;
				preg_match('/(.*?)(User)/',$info['error']['message'],$match);
				if($match)
					$is_user = 1;
				$update[] = array(
					'_id' => $pages[$key]['_id'],
					'error' =>1,
					'status'=>1,
					'is_user' =>1,
					'code' => intval($info['error']['code']),
					'message' => $info['error']['message'],
					'time_scan' => $time
				);
				continue;
			}
			
			$picture = '';
			if(isset($info['picture']['data']['url']))
				$picture = $info['picture']['data']['url'];
			if(isset($info['picture']))
				$picture = $info['picture'];
			
			$update[] = array(
				'_id' => $pages[$key]['_id'],
				'page_name' => $info['name'],
				'page_like' => intval($info['fan_count']),
				'page_picture' => $picture,
				'feed' => $info['feed']['data'],
				'status'=>1,
				'time_scan' => $time
			);	
		}
		$this->mongo_db->batch_update('fb_page_tmps',$update);
	}
	public function getToken()
	{
		return array('EAAAAAYsX7TsBAPWKEqB3S3zc9lz4cTVfiH1yTRJ8GexEgknIJn6r4oMVnB2Gr67JeRPYNiPvwvO7NnFl3U7HO9RZBzfPtsgu5m54TyZBqjY9qeWHQ3UZAYYZB5PAXIxnftKMostZAtwyGvzccmCXG2X0LiYJfTZCYpp7gglCiVzgZDZD','EAAAAAYsX7TsBALnpPVPamaXJ1PXac2iyyUtZBGgx6N5O9TIa7oU1bbnnk6tZAJngsqm1rBXW3RpKFTuUGjhCS2aZB9HGzOYiHWRPlZAy6ZBsveVVHXVaQ7Kvs5a4D5ZAo6TAXr1CsFO5KhOyoANZCOZAdyF7IsAqcOG5ABlDmZBT5FgZDZD');
	}
}