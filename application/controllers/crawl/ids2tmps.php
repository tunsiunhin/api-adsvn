<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Ids2tmps extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('mongo_db','curl'));
	
 	}
	public function index()
	{
		echo '<pre>';
		$tokens = $this->getToken();
		$data = $this->mongo_db->where(array('status'=>0,'error'=>0))->limit(20)->get('fb_post_ids'); // data post ids get
		$posts = array(); // result post get from api facebook
		$update = array(); // update back to post ids
		$insert = array(); // insert new to postTmp
		if(!$tokens || !$data)
			exit;
		
		foreach($data as $row)
		{
			$queries[] = array(
				'method'=>'GET',
				'relative_url' =>$row['post_id'].'?fields=from{name,picture},reactions.limit(0).summary(1),comments.limit(1).summary(1),shares,full_picture,created_time,message,type,child_attachments'
			);
		}
		
		foreach($tokens as $token)
		{
			$res = $this->curl->fb_call('?batch='.json_encode($queries).'&method=post&access_token='.$token);
			
			if(isset($res['error']))
			{
				// cap nhat lai token
				continue;
			}	
			else
			{
				$posts = $res;
				break;
			}
				
		}

		if(!$posts)
			exit();
			
		
		foreach($posts as $key => $post)
		{
			$info = json_decode($post['body'],true);

			if($post['code'] == 400)
			{
				$update[] = array(
					'_id' => $data[$key]['_id'],
					'error' =>1,
					'status'=>1,
					'code' => intval($info['error']['code']),
					'message' => $info['error']['message']
				);
				continue;
			}
			
			$update[] = array(
				'_id' => $data[$key]['_id'],	
				'status'=>1
			);
			$page_name    = isset($info['from']['name']) ? $info['from']['name'] : '';
			
			$page_picture = '';
			if(isset($info['from']['picture']['data']['url']))
			{
				 $page_picture =  $info['from']['picture']['data']['url'];
			}
			if(isset($info['from']['picture']))
			{
				$page_picture = $info['from']['picture'];
			}
			
			$like     = isset($info['reactions']['summary']['total_count'])? intval($info['reactions']['summary']['total_count']) : 0;
			$comment  = 0;
			if(isset($info['comments']['summary']['total_count']))
				$comment = $info['comments']['summary']['total_count']; 
			if(isset($info['comments']['count']))
				$comment = $info['comments']['count'];
				
			$text     = isset($info['message']) ? $info['message'] : '';
			$source	  = isset($info['source']) ? $info['source'] : 1;	
			$share    = isset($info['shares']['count']) ? intval($info['shares']['count']) : 0;
			$img      = isset($info['full_picture']) ? $info['full_picture'] : '';
			$datepost = strtotime($info['created_time']);
			if($info['type'] == 'photo')
			{
				$fbtype  = 1;
			}else if($info['type'] == 'video')
			{
				$fbtype  = 2;
			}
			if(!$img || !$fbtype)
				continue;
			if($like < 100 && $comment < 50)
				continue;
			// Bỏ các bài post Cũ
			//if($datepost < 1483239742)
				//continue;
			$tmp = array(
				'post_id' => $data[$key]['post_id'],
				'page_id' => $data[$key]['page_id'],
				'page_name'    => $page_name,
				'page_picture' => $page_picture,
				'picture'      => $img,
				'content'      => $text,
				'likes_int'    => intval($like),
				'comments_int' => intval($comment),
				'shares_int'   => intval($share),
				'fbtype'   => intval($fbtype),
				'datepost' => intval($datepost),
				'task'     => $data[$key]['task'],// task temp
				'error'       => intval(0),
				'status'      => intval(1),
				'approved'    => intval(1),// Tự động duyệt , sau này duyệt cơm bỏ cái này = 0
				'source'	  => $data[$key]['source'], // source task
				'picture_amz' => intval(0)
			);
			
			$engage = $this->engage($datepost,$like,$comment,$share);
			
			$insert[] = array_merge($tmp,$engage);
			
		}
		$this->mongo_db->batch_update('fb_post_ids',$update);
		if($insert)
		{
			$this->mongo_db->batch_insert('fb_post_tmps',$insert);
			$this->mongo_db->batch_insert('fb_posts',$insert);
		}
		
		
	}

	private function engage($datepost,$like,$comment,$share)
	{
		$time   = time();
		//$engage = ($like > 0) ? ceil($comment / $like * 100) : 0;	
		$hour   = round(($time - $datepost)/3600,2);
		$day    = round(($time - $datepost)/86400,2);
		$trend  = round(($like+$comment+$share)/(($hour+2)^1.5));
		
		if($day > 90){
			$level = 3650 * 86400;
		}
		elseif($day > 60){
			$level = 9 * 86400;	
		}
		elseif($day > 30){
			$level = 5 * 86400; 	
		}
		elseif($day > 15){
			$level = 	3 * 86400;
		}
		elseif($day > 7){
			$level = 1 * 86400;	
		}
		else{
			$level = 1/2 * 86400;	
		}
		
		$result = array(
			//'engagements_int' => intval($engage),
			'trend' => intval($trend),
			'next_update' => intval($time + $level),
			'last_update' => intval($time)
		);
		
		return $result;
	}
	public function getToken()
	{
		return array('EAAAAAYsX7TsBAPWKEqB3S3zc9lz4cTVfiH1yTRJ8GexEgknIJn6r4oMVnB2Gr67JeRPYNiPvwvO7NnFl3U7HO9RZBzfPtsgu5m54TyZBqjY9qeWHQ3UZAYYZB5PAXIxnftKMostZAtwyGvzccmCXG2X0LiYJfTZCYpp7gglCiVzgZDZD','EAAAAAYsX7TsBALnpPVPamaXJ1PXac2iyyUtZBGgx6N5O9TIa7oU1bbnnk6tZAJngsqm1rBXW3RpKFTuUGjhCS2aZB9HGzOYiHWRPlZAy6ZBsveVVHXVaQ7Kvs5a4D5ZAo6TAXr1CsFO5KhOyoANZCOZAdyF7IsAqcOG5ABlDmZBT5FgZDZD');
	}
	public function idsToPages()
	{
		$limit = $this->input->get('limit') ?  $this->input->get('limit') : 1000;
		$time = time();
		$posts = $this->mongo_db->select(array('page_id'))->where(array('status'=>0))->limit($limit)->get('fb_post_ids');
		$pages = array();
		if(!$posts)
			exit();
		foreach($posts as $key => $post)
		{
			$pages[] = array(
			'page_id'=>$post['page_id'],
			'status' => 0,
			'error' => 0,
			'is_user' => 0,
			'time_saved' => intval($time)
			);
			$posts[$key]['status'] = 1;
		}
		$this->mongo_db->batch_insert('fb_page_tmps',$pages);
		$this->mongo_db->batch_update('fb_post_ids',$posts);
	}
}
?>