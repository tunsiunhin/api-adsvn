<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $data = array();
	public $admin;
	public $level;
	public function __construct()
	{
		parent::__construct();
		session_start();
		$this->load->helper(array('string','cookie'));
		$this->check_login();
		
		
 	}
	
	public function check_login()
	{
		if(!isset($_SESSION['admin']) || !isset($_SESSION['level']) || !isset($_SESSION['time_day']) || !isset($_SESSION['time_month'])) {
			redirect(base_url().'login');
			exit;
		}
		
		$this->admin = $_SESSION['admin'];
		
		
		$this->level = $_SESSION['level'];
		
	}
	
	public function view($view)
	{
		$this->data['view'] = $view;
		$this->load->view('template/main',$this->data);
	}
}
class MY_ApiController extends CI_Controller{
	public $data = array();
	public $user_id,$user_level = 0;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('response_helper'));
		$this->check_token();
 	}
	private function check_token()
	{
		$key = 'QGYhm3jsuczmPrymJyDxcrLL';
		$token = $this->input->get('token');
		$info = $this->input->get('user_info');
		if(!$token && !$info)
		{
			exit(response(100,array('error'=>array('messsage'=>'Anauthorized'))));
			exit();
		}
			
		$verify = sha1($info.$key);
		
		if(strcmp($token,$verify) == 0)
		{
			$user_info = json_decode(base64_decode($info),true);
			
			if($user_info['created'] < (time() - 600))
			{
				exit(response(100,array('error'=>array('messsage'=>'Token has expired'))));
				exit();
			}
				
			$this->user_id     = intval($user_info['user_id']);
			$this->user_level = intval($user_info['created']);
		}else
		{
			exit(response(100,array('error'=>array('messsage'=>'Anauthorized'))));
			exit();
		}
	}
	private function check_cookie()
	{
		
		if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_email']) || !isset($_SESSION['user_level'])){
			echo '{"code":104,"error":"Required to request this resource"}';
			exit();
		}
		
		$this->user_id    = $_SESSION['user_id'];
		$this->user_email = $_SESSION['user_email'];
		$this->user_level = $_SESSION['user_level'];
		
	}
	
}
?>