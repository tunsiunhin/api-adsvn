<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Model extends CI_Model  {
	public $isAdmin;
	public function __construct()
	{
		parent::__construct();
		//$this->check_login();		
 	}
 	public function check_login()
	{
		if(!$this->session->userdata('isAdmin')){
			redirect(base_url().'login');
			exit();
		}
		$this->isAdmin = $this->session->userdata('isAdmin');
	}
	private function redirect($url, $statusCode = 303)
	{
		   header('Location: ' . $url, true, $statusCode);
		   die();
	}
 }
	
?>