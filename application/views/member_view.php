<style>
.acc-filter{
	border-bottom:1px solid #eee;
	padding-bottom:20px;
	padding-top:15px
}
.acc-filter>div{
		float:left;
		margin-right:10px;	
}

.acc-add-p{
	float:right !important;	
}
.text-search{
	width:250px	
}
.input-account-search{
	position:relative;	
}
.input-account-search i{
	position:absolute;
	right:10px;
	top:10px;
	color:#ccc	
}
.table-acc td{
	vertical-align:middle !important	
}
.btn-pause-acc{
	width:66px	
}
.acc-filter .mem-statistic{
	float:right;
	vertical-align:middle;
	line-height:34px;	
}

</style>


<div class="clearfix acc-filter">
    <div class="input-account-search">
        
        <div class="dropdown">
            <i class="fa fa-search"></i>
            <input class="form-control text-search dropdown-toggle" data-toggle="dropdown" placeholder="Search Member" type="search">
            
            <div class="dropdown-menu">
                <div style="padding:5px 10px">
                Press Enter to search
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">All account
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="/account">All account</a></li>
            <li><a href="/account?status=1">Active account</a></li>
            <li><a href="/account?status=2">Error account</a></li>
            <li><a href="/account?status=4">Account deleted</a></li>
          </ul>
        </div>
    </div>
    
    
   <div class="mem-statistic">
        <span>Total <strong>0</strong></span>
    </div>
    
    
</div>


<div>
        <table class="table table-striped table-acc">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Expire</th>
                   
                    <th style="width:180px">Action</th>
                </tr>
            </thead>
            <tbody>
            
            <?php foreach($data as $key => $row) 
			{
			
			?>
                <tr data-id="<?=$row['user_id']?>">
                   	<td><?=$row['user_id']?></td>
                    <td><?=$row['user_email']?></td>
                    <td><?=$row['user_level']?></td>
                    <td><?=date('d/m/Y',$row['user_expire'])?></td>
                    <td>
                    	<button class="btn btn-default">Active</button>
                    </td>
                </tr>
             <?php } ?> 
            </tbody>
        </table>
</div>
