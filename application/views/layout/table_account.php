<?php
foreach($data as $key => $row) 
{
		$cookies = json_decode($row['cookie'],true);
		$export = array();
		foreach($cookies as $name => $value) 
		{
			$export[] = array
			(
				"domain"=> ".facebook.com",
				"hostOnly"=> false,
				"httpOnly"=> true,
				"secure" => true,
				"name"=> $name,
				"path"=> "/",
				"value"=> $value
			);
				
		}
?>
		<tr data-id="<?=$row['id']?>">
			<td><?=$key+1?></td>
			<td><?=$row['fbid']?></td>
			<td><?=$row['proxy_name']?></td> 
			<td><?=$row['page_count']?></td>
            <td><?=$row['scan_type']?></td>
			<td><?=date('H:i d/m',$row['next_run'])?></td>
			<td class="acc-status">
				<?php
					if($row['status'] == 0)
						echo'<span class="label label-danger">Account error</span>';
					elseif($row['status'] == 4)
						echo'<span class="label label-danger">Checkpoint</span>';	
					elseif($row['status'] == 5)
						echo'<span class="label label-danger">Limit</span>';		
					else if($row['proxy_status'] == 0)
						echo'<span class="label label-danger">Proxy error</span>';
					else
						echo'<span class="label label-success">Active</span>';
					if($row['stop'] == 1)
						echo'<br /><span class="label label-warning">Stopped</span>';	
				?>
				
			</td>
			<td class="hidden">
				<div class="acc-password"><?=$row['password']?></div>
				<div class="acc-cookie"><?=json_encode($export)?></div>
				<div class="acc-proxy"><?=$row['host']?></div>
				<div class="acc-fbid"><?=$row['fbid']?></div>
				<div class="acc-useragent"><?=$row['user_agent']?></div>
			</td>
			<td>
				<button class="btn btn-default btn-pause-acc"><span><?=($row['stop']==0)? 'Pause' : 'Start'?></span></button>
				<div class="dropdown" style="">
				<button type="button" class="btn btn-default atc-edit dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></button>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a class="btn-show-acc" href="javascript:;">Show data</a></li>
						<li><a class="btn-check-acc" href="javascript:;">Check live</a></li>
						<li><a class="btn-language" href="javascript:;"  data-status="0">Change language</a></li>
				  </ul>
				</div>
			</td>
		</tr>
 <?php } ?> 