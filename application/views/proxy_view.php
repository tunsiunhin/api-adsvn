<ol class="breadcrumb">
  <li><a href="/">Dashboard</a></li>
  <li class="active">Proxy manager</li>
</ol>
<style>
.proxy-filter{
	padding-bottom:20px
}	
</style>
<div class="clearfix proxy-filter">
    <div class="acc-add-proxy" style="float:right;">
      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_multi_proxy">Add many proxys</button>
    </div>
 </div>
     
<div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Host</th>
                <th>Auth</th>
                <th>Added date</th>
                <th>Status</th>
                <th style="width:150px">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($data as $key => $row) { ?>
        	<tr data-id="<?=$row['id']?>">
                    <td><?=$key+1?></td>
                    <td><?=$row['name']?></td>
                    <td><?=$row['host']?></td>
                    <td><?=$row['auth'] ? $row['auth'] : 'N/a'?></td>
                    <td><?=date('H:i',$row['created'])?></td>
                    <td>
                    	<?php
                        	if($row['status'] == 0)
								echo'<span class="label label-danger">Error</span>';
							else
								echo'<span class="label label-success">Active</span>';
						?>
                    	
                    </td>
                    <td>
                    	<button class="btn btn-default btn-sm btn-check-proxy">Check</button>
                        <button class="btn btn-default btn-sm">Edit</button>
                    </td>
                </tr>
        <?php } ?>
        </tbody>
    </table>    
</div> 
<div class="modal fade" id="add_proxy" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><strong>ADD NEW PROXY</strong></h4>
             </div>
             <div class="modal-body">
             	<div class="form-group">
            		<h5><strong>Name</strong></h5>
                	<input class="form-control proxy-name" type="text" placeholder="Enter proxy name...." />
            	</div>       
            	<div class="form-group">
                	<h5><strong>Proxy</strong></h5>
                    <input class="form-control proxy-proxy" type="text" placeholder="Example 163.44.167.101:8181"/>
                </div>
                <div class="form-group">
                	<h5><strong>Auth</strong></h5>
                    <input class="form-control proxy-proxy" type="text" placeholder="user:pass"/>
                </div>
             </div>
             
             <div class="modal-footer">
             	<button type="button" class="btn btn-primary act-add-proxy" data-endpoint="add">Apply</button>
             </div>
          </div>
    </div>
</div> 

<div class="modal fade" id="add_multi_proxy" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
          <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><strong>ADD NEW PROXY</strong></h4>
             </div>
             <div class="modal-body">
             	<div class="form-group">
            		<h5><strong>Name</strong></h5>
                	<input class="form-control proxy-name" type="text" placeholder="Proxy name" />
            	</div>       
            	<div class="form-group">
                	<h5><strong>Host</strong></h5>
                    <textarea class="form-control proxy-host" rows="7" placeholder="Proxy list"></textarea>
                </div>
                <div class="form-group">
                	<h5><strong>Auth</strong></h5>
                    <input class="form-control proxy-auth" type="text" placeholder="user:pass"/>
                </div>
             </div>
             
             <div class="modal-footer">
             	<button type="button" class="btn btn-primary act-add-m-proxy">Apply</button>
             </div>
          </div>
    </div>
</div> 

<script>
$(document).ready(function(e) {
	
	$('.act-add-m-proxy').click(function(e) 
	{
        var name = $('#add_multi_proxy').find('.proxy-name').val();
		var host = $('#add_multi_proxy').find('.proxy-host').val();
		var auth = $('#add_multi_proxy').find('.proxy-auth').val();
		
		if(!name || !host) {
			alert('Field invalid!');
			return false;	
		}
		
		$this = $(this);
		$this.prop('disabled',true);
		
		$.ajax({
			url:'adssuccess/proxy/add',
			type:'post',
			data:{'name':name,'host':host,'auth':auth},
			dataType:'json',
			beforeSend: function(){},
			success:function(res) 
			{
				$this.prop('disabled',false);
				if(res.code == 200) {
					location.reload();	
				}	
				else
					alert(res.message);
			}	
		});
    });
	
	$('.btn-check-proxy').bind('click',function(){
		
		var proxy_id = $(this).parents('tr').attr('data-id');
		$this = $(this);
		$this.prop('disabled',true);
		$.ajax({
			url:'adssuccess/proxy/check',
			data:{'proxy_id':proxy_id},
			type:'POST',
			dataType:"json",
			success: function(res) 
			{
				$this.prop('disabled',false);
				alert(res.message);
			}
		});	
	});  
	
});

</script> 
