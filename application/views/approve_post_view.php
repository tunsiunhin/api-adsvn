Count : <b><?=$count?></b>
<div class="list-niche" style="width: 155px;position:absolute;display: none;">
<select class="selectpicker" multiple data-size="10" data-width="100%" data-live-search="true" title="Niche">
	<?php 
        foreach($niches as $row) 
        {
            if($row['category_id'] == 0){
                echo '<optgroup label="'.strtoupper($row['name']).'">';
                foreach($niches as $k => $r) 
                {
                    if($r['category_id'] == $row['id'])
                        echo'<option value="'.$r['id'].'">'.ucfirst($r['name']).'</option>';
                }
                echo '</optgroup>';
                    
                //unset($niches[$k]);
            }
                
        }
      ?>
</select>
</div>


<style>
	.btn-niche{
		position:relative;	
		display:inline-block;
	}
	.btn-niche a{
		position:absolute;
		width:100%;
		height:100%;
		z-index:100;
		opacity:0.5;
		top:0;
		left:0;
		background:#000;
		display:none;
	}
	.btn-niche button{
		z-index:1;
		width:155px;
			
	}
</style>
<style>
	.all-data
	{
		padding-top:30px;
	}
	.col-md-4
	{
		margin-top:10px;
	}
	.col-object
	{
		border:solid 3px #ccc;
	}
	.col-picture,.col-picture>img
	{
		height:270px;
		width:100%;
	}
	.col-reaction
	{
		padding:5px 0;
		text-indent:6px;
	}
	.col-info
	{
		padding-top: 5px;
	        display: inline-block;
    		white-space: nowrap;
    		text-indent: 6px;
    		width: 100%;
    		overflow: hidden;
    		text-overflow: ellipsis;
	}
	.col-info p
	{
		text-indent: 24px;
		color: #827c7c;
		padding-top: 0px;
		font-size: 13px;
		position: absolute;
    		top: 337px;
    		left: 20px;
	}
	.col-text
	{
		height: 99px;
		overflow: hidden;
		text-overflow: ellipsis;
		padding: 0 13px;
	}
	.col-button
	{
		text-align:center;
		padding:10px 0;
	}
</style>


<div class="all-data row">
   <?php foreach($fbposts as $post){?>
         <div class="col-md-4 active-post not-approved">
            <div class="col-object" data-id="<?=$post['_id']?>" data-page="<?=$post['page_id']?>">
            	 <a target="_blank" href="https://fb.com/<?=$post['post_id']?>">
                    <div class="col-picture">
                       <img class="img-responsive" src="<?=$post['picture']?>">
                    </div>
                </a>
                <div class="col-reaction">
                     <i class="fa fa-thumbs-up"></i><b><?=$post['likes_int']?> </b>
                     <i class="fa fa-comments"></i><b><?=$post['comments_int']?> </b>
                     <i class="fa fa-share"></i><b><?=$post['shares_int']?> </b>
                </div>
                <div class="col-info">
               	 <a target="_blank" href="https://fb.com/<?=$post['page_id']?>">
                    <img class="img-circle" width="40" height="40" src="https://graph.facebook.com/<?=$post['page_id']?>/picture"> <b><?=$post['page_name']?></b></a>
                    <p><i class="fa fa-globe"></i> <?=date('d/m/Y',$post['datepost'])?></p>
                </div>
                <div class="col-text">
                	<?=substr(strip_tags($post['content']),0,100)?>
                </div>
                
                <div class="col-button">
                    <button class="btn btn-sm btn-success btn-action"><i class="far fa-foursquare"></i>Approved</button>
                   
                </div>
            </div>
        </div>
   <?php } ?>

</div>
<div style="margin-top: 20px; margin-left: 13px">
    <button class="btn btn-sm btn-danger denied_all"><i class="fa fa-refresh"></i> Load new data</button>
</div>

<script>
	$(document).ready(function()
	{
		
		
		
		$('.btn-action').click(function(){
				var id = $(this).parents('.col-object').attr('data-id');
				var page_id = $(this).parents('.col-object').attr('data-page');
				
				var _this = $(this);
				_this.prop('disabled',true);
				$.ajax({
					url:'adssuccess/post/approve',
					type:'post',
					data:{'id':id,'page_id':page_id},
					dataType:"json",
					success:function(res)
					{
						_this.prop('disabled',false);
						
						if(res.code == 400) {
							alert(res.message);	
						}
						else {
							_this.parents('.col-object').css('background','#398439');
							_this.parents('.active-post').removeClass('not-approved');
						}
					}
				});
			
		});
		
        $('.denied_all').click(function() {
			
			var ids = [];
			
			$('.not-approved').each(function() {
				ids.push($(this).children().attr('data-id'));
			});
			
			$('.denied_all').prop('disabled',true);
			
			$.ajax({
				url:'adssuccess/post/denined',
				type:'post',
				data:{'ids':ids},
				success:function(res)
				{
					$(window).scrollTop(0);
					$('.denied_all').prop('disabled',false);
					
					location.reload();
				}
			});
		});
	});
</script>
