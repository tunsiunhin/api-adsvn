 <?php  function fm_num($num){
		if($num > 1000 && $num < 1000000)
			return round($num/1000,1).'K';
		else if($num >= 1000000)
			return round($num/1000000,1).'M';
		else
			return $num;
 } ?>
<style>
.page-filter{
	padding-top:15px;	
	padding-bottom:10px;
	
}
.page-filter>div{
	float:left	
}
.post-info div{
	display:inline-block;
	width:180px;
	margin-right:5px;
	height:300px;
	border:1px solid #ddd;
	border-radius:2px;
	font-size:12px;
	overflow:hidden;
}
.post-info img{
	width:100%;
	height:160px;	
}
</style>
<div class="clearfix page-filter">
	<div>
    	Hello <strong><?=$admin['name']?></strong>
    </div>
    <div style="float:right">
    	<span style="margin-right:15px">Total <strong><?=number_format($admin['approved_total'])?></strong></span>
    	<span style="margin-right:15px">Today <strong><?=number_format($admin['approved_day'])?></strong></span>
        <span>This month <strong><?=number_format($admin['approved_month'])?></strong></span>
    </div>
</div>	
<div class="alert alert-warning" role="alert" style="margin-bottom:5px">
	Cố gắng xét kỹ trước khi denined. Có để lại rồi hỏi admin trên facebook
</div>

<div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th style="width:60px">#</th>
                <th style="width:;">Page Name</th>
                <th style="width:60px">Like</th>
                <th style="width:640px">Posts</th>
                <th style="width:100px">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($pages as $p){?>
                <tr data-id="<?=$p['_id']?>">
                <td> <a target="_blank" href="https://fb.com/<?=$p['id_page']?>" ><img class="img-circle" width="50" height="50" src="<?=$p['page_picture']?>" /></a></td>
                <td class="align-right"><?=$p['page_name']?></td>
                <td><?=fm_num($p['page_like'])?></td>
                <td class="post-info">
                	<?php
						foreach($p['posts']['list_post'] as $key => $row) 
						{
							if($key == 3) {
								break;	
							}
							echo'<div>';
								echo'<p><img src="'.$row['picture'].'" />';
								echo'<p>'.$row['content'].'</p>';
							echo'</div>';
							
							
						}
					
					?>
                </td>
                <td>
                	
                    <button class="btn btn-success btn-sm btn-action" data-action="1"><i class="fa fa-check"></i> Accept</button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div class="list-post">
    
</div>

<div class="" style="padding-top:15px;padding-bottom:20px;">
	<button class="btn btn-danger btn-denied-all">Denied all</button>
</div>

<script>
	$(document).ready(function()
	{
			var parent,btnAction,typePage,pageId;
			
			
			$('.btn-action').click(function()
			{
				var id = $(this).parents('tr').attr('data-id');
				
				var parent = $(this).parents('tr');
				
				$.ajax({
					url:'adssuccess/page/approve',
					type:'post',
					data:{'id':id,'product':1},
					beforeSend: function()
					{
						parent.find('.btn-action').prop('disabled',true);
					},
					success: function(res){
						parent.find('.btn-action').prop('disabled',false);
						parent.css('background','#5cb85c');
						parent.addClass('actived');
					}
				});
						
			});
			
		});
		
		
		$('.btn-denied-all').click(function(e) {
			
           	var denieds = [];
			
			$('tbody tr:not(".actived")').each(function() {
           		denieds.push($(this).attr('data-id')); 
        	});  
			
			$.ajax({
				url:'adssuccess/page/denied',
				type:'post',
				data:{'ids':denieds},
				dataType:'json',
				success: function(res){
					$(window).scrollTop(0);
					location.reload();
				}
			});
        });
</script>