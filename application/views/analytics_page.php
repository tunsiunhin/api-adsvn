<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Analytics</h1>
    </div>
</div>
<div>
<div>
	<h4>Total </h4>
</div>
	<table class="table table-striped table-hover">
		<thead>
        	<tr>
            	<th>#</th>
                <th>#Task</th>
                <th>Count Post Task</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($analytics as $key => $ana){ ?>
        	<tr>
            	<td>#</td>
            	<td><?=$key?></td>
                <td><?=number_format($ana)?></td>
            </tr>
          <?php } ?>
        </tbody>
	</table>
    
    <h4>Total Per/day</h4>
     <div class="panel panel-default">
                        <div class="panel-heading">
                            Bar Chart
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-bar-per-day"></div>
                        </div>
                        <!-- /.panel-body -->
      </div>
    <h4>Total</h4>
     <div class="panel panel-default">
                        <div class="panel-heading">
                            Bar Chart
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-bar"></div>
                        </div>
                        <!-- /.panel-body -->
      </div>
      <script>
        $(function() {
	    Morris.Bar({
        element: 'morris-bar',
        data: [<?php foreach($analytics_per_day as $p){?>
		{
            y: <?=date('d',$p['time_update'])?>,
            a: <?=$p['page_active_crawl']?>,
            b: <?=$p['crawl_id_from_page']?>,
			c: <?=$p['crawl_post_from_page']?>,
			d:<?=$p['posts_active_auto_accpect']?>
        },
		<?php }?>],
        xkey: 'y',
        ykeys: ['a', 'b', 'c', 'd'],
        labels: ['Page Active', 'Crawl Id Page', 'Crawl Post Page', 'Auto Accept'],
        hideHover: 'auto',
        resize: true
    	});
		Morris.Bar({
        element: 'morris-bar-per-day',
        data: [<?php foreach($analytics_per_day as $key => $p){
			if($key == 0)
				continue;
			?>
		{
            y: <?=date('d',$p['time_update'])?>,
            a: <?php echo $analytics_per_day[$key]['page_active_crawl'] - $analytics_per_day[($key-1)]['page_active_crawl'];?>,
            b: <?php echo $analytics_per_day[$key]['crawl_id_from_page'] - $analytics_per_day[($key-1)]['crawl_id_from_page']?>,
			c: <?php echo $analytics_per_day[$key]['crawl_post_from_page'] - $analytics_per_day[($key-1)]['crawl_post_from_page']?>,
			d: <?php echo $analytics_per_day[$key]['posts_active_auto_accpect'] - $analytics_per_day[($key-1)]['posts_active_auto_accpect']?>
        },
		<?php }?>],
        xkey: 'y',
        ykeys: ['a', 'b', 'c', 'd'],
        labels: ['Page Active', 'Crawl Id Page', 'Crawl Post Page', 'Auto Accept'],
        hideHover: 'auto',
        resize: true
    	});
    
});

      </script>
</div>