<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Robot</h1>
    </div>
</div>

<div>
	<div class="form-group">
    	<label>Task</label>
        <select class="form-control task">
        	<option value="">Choose</option>
        	<option value="cron1">cron1</option>
            <option value="cron2">cron2</option>
            <option value="cron3">cron3</option>
            <option value="cron4">cron4</option>
            <option value="cron5">cron5</option>
            <option value="cron6">cron6</option>
            <option value="cron7">cron7</option>
            <option value="cron8">cron8</option>
            <option value="cron9">cron9</option>
            <option value="cron10">cron10</option>
            <option value="cron11">cron11</option>
            <option value="cron12">cron12</option>
            <option value="cron13">cron13</option>
        </select>
    </div>
    <div class="form-group">
    	<label>Time</label>
        <select class="form-control time">
        	<option value="">Choose</option>
        	<option value="1">This Month</option>
            <option value="2">Last Month</option>
            <option value="3">This Year</option>
        </select>
    </div>
     <div class="form-group">
    	<label>Type</label>
        <select class="form-control type_id">
        	<option value="">Choose</option>
        	<option value="1">Platform</option>
            <option value="2">Shopify</option>
        </select>
    </div>
    <div class="form-group">
    	<label>Country</label>
        <select class="form-control country">
        	<option value="">Choose</option>
        	<option value="1">US</option>
        </select>
    </div>
    <div class="form-group">
    	<label>Link</label>
        <input type="" class="form-control link" />
    </div>
    <div class="form-group">
        <label>Source</label>
        <select class="form-control source">
            <option value="">Choose</option>
            <option value="1">Post</option>
            <option value="2">Photos</option>
        </select>
    </div>
    <div class="form-group">
    	<label>Keyword</label>
        <textarea type="" class="form-control keyword" rows="10"></textarea>
    </div>
    <div class="form-group text-right">
    	<button class="btn btn-success add-new" value="2"><i class="fa fa-refresh"></i> Refresh</button>
        <button class="btn btn-primary add-new" value="1"><i class="fa fa-refresh"></i> Add New</button>
    </div>    
</div>
	<div class="row">
	<div class="panel panel-default">
	<div class="panel-heading" style="overflow: hidden">
        <div style="float: left"><i class="fa fa-list"></i> List Robot</div>
        <div style="float:left; margin-left: 20px">
            <select id="task" onchange="load_robot()" name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm">
                <option value="">All</option>
                <option value="cron1">cron1</option>
                <option value="cron2">cron2</option>
                <option value="cron3">cron3</option>
                <option value="cron4">cron4</option>
                <option value="cron5">cron5</option>
                <option value="cron6">cron6</option>
                <option value="cron7">cron7</option>
                <option value="cron8">cron8</option>
                <option value="cron9">cron9</option>
                <option value="cron10">cron10</option>
                <option value="cron11">cron11</option>
                <option value="cron12">cron12</option>
                <option value="cron13">cron13</option>
            </select>
        </div>
    </div>
    <div class="panel-body">
    <table class="table table-striped table-hover">
    	<thead>
        	<tr>
                <th>STT</th>
            	<th>Task</th>
                <th>Link</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="load-robot">
        	<?php
            $stt = 1;
            foreach($robot as $r){ ?>
            	<tr id="<?= $r['_id']?>">
                    <td><?= $stt ?></td>
                	<td><?=$r['task']?></td>
                   	<td><a href="<?=$r['link']?>" target="_blank"><?=$r['subLink']?></a></td>
                    <td><button onclick="del('<?= $r['_id']?>');" type="button" class="btn btn-danger">Delete</button></td>
                </tr>
            <?php $stt++; } ?>
        </tbody>
    </table>
    </div>
   <div>
</div>
<script>
$('.add-new').bind('click',function(e) 
{
	_this   = $(this);
	_link   = $('.link').val();
	task    = $('.task').val();
	keyword = $('.keyword').val();
	country = $('.country').val();
	time    = $('.time').val();
	type    = $(this).val();
	type_id = $('.type_id').val();
	source = $('.source').val();
	if(!_link && !type)
		alert('Plz check type');
	
	post = {'task':task,'link':_link,'keyword':keyword,'time':time,'country':country,'type_id':type_id,'type':type,'source':source};
	
	$.ajax({
		url:'/ajax/create-robot',
		type:'post',
		data:post,
		beforeSend: function(){
			_this.find('i').addClass('fa-spin');
		},
		success:function(res){
			if(res == 1)
				alert('success');
			_this.find('i').removeClass('fa-spin');
		}	
	});
});
function load_robot() {
    var task = $('#task').val();
    $.ajax({
        url: 'main/load_link',
        type: 'post',
        data: {'task' : task},
        success: function (res) {
            var html = '';
            var stt = 1;
            res = JSON.parse(res);

            res.forEach(function(v) {
                html += '<tr id="'+v._id.$id+'">';
                html += '<td>'+stt+'</td>';
                html += '<td>'+v.task+'</td>';
                html += '<td><a href="'+v.link+'" target="_blank">'+v.subLink+'</a></td>';
                html += '<td><button onclick="del(\''+v._id.$id+'\');" type="button" class="btn btn-danger">Delete</button></td>';
                html += '</tr>';
                stt++;
            });

            //console.log(html);
            $('#load-robot').html(html);
        }
    })
}

function del(id){
    var conf = confirm("B?n c� ch?c mu?n x�a?");
    if(conf == 'false')
        return false;
    $.ajax({
        url: 'main/del_link',
        type: 'post',
        data: {'id' : id},
        success: function (res) {
            $('#'+id).remove();
        }
    });
}
</script>