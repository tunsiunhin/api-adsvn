<?php
foreach($data as $row) {
	
	unset($row['target']['orderID']);
	unset($row['exec']['orderID']);
?>
	<tr>
    	<td><?=$row['ordType']?></td>
        <td>
        	<span>Balance <strong><?=round($row['wallet1']/100000000,4)?> </strong> </span>
            <span> Qty <strong><?=$row['target']['orderQty']?></strong></span><br />
			<div class="bg-">
            	<?php foreach($row['target'] as $k => $val) {
					echo '"'.$k .'"'. ':' . $val . '<br />';	
				}?>
            </div>    
        </td>
        <td>
        	<span>Balance <strong><?=round($row['wallet2']/100000000,4)?> </strong> </span>
            <span> Qty <strong><?=$row['exec']['orderQty']?></strong></span><br />
			<div class="bg-">
            	<?php foreach($row['exec'] as $k => $val) {
					echo '"'.$k .'"'. ':' . $val . '<br />';	
				}?>
            </div>    
        </td>
        <td><?=date('H:i d/m',$row['created'])?></td>
        <td><button class="btn btn-default acc_detail">Detail</button></td>
    </tr>
<?php			
}
?>