
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Keyword VI</h1>
	</div>
	<div>
		<div class="form-group">
			<label>Category</label>
			<select class="form-control category">
				<?php foreach ($categories as $row): ?>
					<option value="<?= $row['id']?>"><?=$row['name']?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label>Link</label>
			<input type="" class="form-control link" name="" value="https://www.facebook.com/search/str/{kw}/stories-keyword/this-year/date/stories/intersect">
		</div>

		<div class="form-group">
			<label>Keyword</label>
			<textarea class="form-control keyword" rows="10"></textarea>
		</div>

		<div class="form-group text-right">
			<button class="btn btn-success add-new" value="add_more">
				<i class="fa fa-refresh"></i>Add More
			</button>
			<button class="btn btn-primary add-new" value="add_new">
				<i class="fa fa-refresh"></i>Add New
			</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.add-new').bind('click',function(e) 
	{
		_this   = $(this);
		_link   = $('.link').val();
		cate_id    = $('.category').val();
		keyword = $('.keyword').val();
		type    = $(this).val();
		
		if(!_link || !keyword)
		{
			alert('Input Error');
			return;
		}
			
		
		post = {'status': type,'link':_link,'keyword':keyword,'category_id':cate_id};
		
		$.ajax({
			url: 'adssuccess/keyword/index',
			type:'POST',
			data:post,
			beforeSend: function(){
				_this.find('i').addClass('fa-spin');
			},
			success:function(res)
			{
				if(res == 'success')
					alert('success');
				_this.find('i').removeClass('fa-spin');
			}	
		});
	});
</script>