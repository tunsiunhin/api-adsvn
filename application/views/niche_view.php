<ol class="breadcrumb">
  <li><a href="/">Dashboard</a></li>
  <li class="active">Niche manager</li>
</ol>
<div role="alert" class="alert alert-info clearfix page-filter" style="margin-bottom:5px">
	<div class="statistic" style="float:left;line-height:34px">
    	<span>Total <strong>0</strong></span>
        
    </div>
    <button class="btn btn-primary" data-toggle="modal" data-target="#nicheModal" style="float:right">Add new niche</button>	
</div><div>
    <table class="table table-striped table-page">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Category</th>
                <th style="width:350px">Child</th>
                <th>Post count</th>
                <th>Status</th>
                <th style="width:140px">Action</th>
            </tr>
        </thead>
        <tbody class="tb-ajax">
        	<?php foreach($niches as $row) {
				if($row['category_id'] == 0 || $row['parent_id'] != 0)
					continue;
			?>
            <tr>
            	<td><?=$row['id']?></td>
                <td><?=$row['name']?></td>
                <td>
                	<?php
						foreach($niches as $key => $c) {
							if($c['id'] == $row['category_id'])
							{
								echo $c['name'];
								//unset($niches[$key]);	
							}	
						}
					?>	
                </td>
                <td>
                	<?php
						foreach($niches as $key => $c) {
							if($c['parent_id'] == $row['id'])
							{
								echo $c['name'] .' - ';
								unset($niches[$key]);	
							}	
						}
					?>
                </td>
                <td><?=number_format($row['post_count'])?></td>
                <td>
				<?php
                	if($row['status'] == 0)
						echo'<span class="label label-warning">Pending</span>';
					else
						echo'<span class="label label-success">Success</span>';
				?>
                </td>
                <td>
                	<button class="btn btn-default">Detail</button>
                </td>
            </tr>
            <?php } ?>
       
        </tbody>
    </table>
    
    <div class="pagi">
    
    </div>
</div>

<div id="nicheModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Niche</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        	<label>List niches</label>
            <textarea class="form-control" rows="7" name="niche_name"></textarea>
        </div>
        <div class="form-group">
        	<label>Parent</label>
            <select class="form-control" name="niche_parent">
            	<option value="">Select parent</option>
                <?php
					foreach($niches as $row) {
						if($row['category_id'] != 0 && $row['parent_id'] == 0)
							echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';	
					}
				?>
            </select>
        </div>
        <div class="form-group">
        	<label>Category</label>
            <select class="form-control" name="niche_category">
            	<option value="">Select category</option>
                <?php
					foreach($niches as $row) {
						if($row['category_id'] == 0)
							echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';	
					}
				?>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success niche_new">Add new</button>
      </div>
    </div>

  </div>
</div>

<script>
$(document).ready(function(e) {
 	$('.niche_new').click(function(e) {
    	
		var niches = $('textarea[name="niche_name"]').val();
		var parent = $('select[name="niche_parent"]').find('option:selected').val();
		var category = $('select[name="niche_category"]').find('option:selected').val();
		if(!niches) {
			return false;	
		}
		$(this).prop('disabled',true);
		$this = $(this);
		request('adssuccess/niche/add',{'niches':niches,'parent':parent,'category':category}).done(function(res){
			$this.prop('disabled',false);
			alert('Success');
		});
	});	   
});

</script>