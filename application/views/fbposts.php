<h2>Count : <b><?=$count?></b></h2>
<div class="all-data" style="overflow: hidden">
   <?php foreach($fbposts as $post){?>
         <div class="col-md-4 active-post" >
            <div class="col-object" data-id="<?=$post['_id']?>" data-page="<?=$post['page_id']?>">
            	 <a target="_blank" href="https://fb.com/<?=$post['post_id']?>">
                    <div class="col-picture">
                       <img class="img-responsive" src="<?=$post['picture']?>">
                    </div>
                </a>
                <div class="col-reaction">
                     <i class="fa fa-thumbs-up"></i><b><?=$post['likes_int']?> </b>
                     <i class="fa fa-comments"></i><b><?=$post['comments_int']?> </b>
                     <i class="fa fa-share"></i><b><?=$post['shares_int']?> </b>
                </div>
                <div class="col-info">
               	 <a target="_blank" href="https://fb.com/<?=$post['page_id']?>">
                    <img class="img-circle" width="40" height="40" src="https://graph.facebook.com/<?=$post['page_id']?>/picture"> <b><?=$post['page_name']?></b></a>
                    <p><i class="fa fa-globe"></i> <?=date('d/m/Y',$post['datepost'])?></p>
                </div>
                <div class="col-text">
                	<?=strip_tags($post['content'])?>
                </div>
                <div class="col-button">
                    <button class="btn btn-sm btn-info btn-action" data-type="2"><i class="fa fa-opencart"></i> Shopify</button>
                    <button class="btn btn-sm btn-success btn-action" data-type="1"><i class="fa fa-foursquare"></i> Plaform</button>
                   
                </div>
            </div>
        </div>
   <?php } ?>

</div>
<div style="margin-top: 20px; margin-left: 13px">
    <button class="btn btn-sm btn-danger denied_all" data-type="1"><i class="fa fa-trash"></i> Denied ALL</button>
</div>
<script>
    var data_ids = new Array();
	$(document).ready(function(){
		$('body').scrollTop(0);
			$('.btn-action').click(function(){
					var type =  $(this).attr('data-type');
					var data_id = $(this).parents('.col-object').attr('data-id');
					var page_id = $(this).parents('.col-object').attr('data-page');
					var _this = $(this);
						$.ajax({
							url:'ajax/process-post',
							type:'post',
							data:{'type':type,'data_id':data_id,'page_id':page_id},
							beforeSend:function()
							{
								_this.prop('disabled',true);
							},
							success:function(res)
							{
								res = res.trim();
								_this.prop('disabled',false);
								_this.parents('.col-md-4').removeClass('active-post');
								if(res == 3)
									_this.parents('.col-object').css('background','#da524f');
								else if(res == 1)
									_this.parents('.col-object').css('background','#5cb85c');
								else if(res == 2)
									_this.parents('.col-object').css('background','#46b8da');

							}
						});
				});
        $('.denied_all').click(function() {
                $('.active-post').each(function (k, v) {
                    data_ids.push($(this).children().attr('data-id'));
                });
                $.ajax({
                    url:'ajax/process-post',
                    type:'post',
                    data:{'type':3,'data_id':data_ids,'page_id':[]},
                    beforeSend:function()
                    {
                        $('.denied_all').prop('disabled',true);
                    },
                    success:function(res)
                    {
                        res = res.trim();
                        $('.denied_all').prop('disabled',false);
                        $('.active-post').children().css('background','#da524f');
//                        _this.parents('.col-md-4').removeClass('active-post');
//                        if(res == 3)
//                            _this.parents('.col-object').css('border','solid 3px #da524f');
//                        else if(res == 1)
//                            _this.parents('.col-object').css('border','solid 3px #5cb85c');
//                        else if(res == 2)
//                            _this.parents('.col-object').css('border','solid 3px #46b8da');

                    }
                });
            });
		});
</script>
<style>
	.all-data
	{
		padding-top:30px;
	}
	.col-md-4
	{
		margin-top:10px;
	}
	.col-object
	{
		border:solid 3px #ccc;
	}
	.col-picture,.col-picture>img
	{
		height:270px;
		width:100%;
	}
	.col-reaction
	{
		padding:5px 0;
		text-indent:6px;
	}
	.col-info
	{
		padding-top: 5px;
	        display: inline-block;
    		white-space: nowrap;
    		text-indent: 6px;
    		width: 100%;
    		overflow: hidden;
    		text-overflow: ellipsis;
	}
	.col-info p
	{
		text-indent: 24px;
		color: #827c7c;
		padding-top: 0px;
		font-size: 13px;
		position: absolute;
    		top: 337px;
    		left: 20px;
	}
	.col-text
	{
		height: 99px;
		overflow: hidden;
		text-overflow: ellipsis;
		padding: 0 13px;
	}
	.col-button
	{
		text-align:center;
		padding:10px 0;
	}
</style>