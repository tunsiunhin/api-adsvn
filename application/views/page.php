 <?php  function fm_num($num){
		if($num > 1000 && $num < 1000000)
			return round($num/1000,1).'K';
		else if($num >= 1000000)
			return round($num/1000000,1).'M';
		else
			return $num;
 } ?>
 <div class="container-fluid main-page">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Page</h1>
                        <p><?php /*?><b>Total : <?=$total?> - Approval : <?=$count?> </b><?php */?></p>
                    </div>
                    <div class="col-lg-12">
                   	<div class="table-responsive">
                    	<table class="table table-striped table-hover">
                        	<thead>
                            	<tr>
                                	<th>#</th>
                                    <th>Page Name</th>
                                    <th>Like</th>
                                    <th>Detail</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($pages as $p){?>
                            		<tr data-id="<?=$p['_id']?>">
                                	<td> <a target="_blank" href="<?=$p['page_url']?>" ><img class="img-circle" width="50" height="50" src="<?=$p['page_picture']?>" /></a></td>
                                    <td class="align-right"><?=$p['page_name']?></td>
                               		<td><?=fm_num($p['page_like'])?></td>
                                    <td><button class="btn btn-default btn-detail">Detail</button></td>
                                    <td><div class="form-group">
                                            <div class="radio">
                                                <label>
                                                    <input class="check-radio" type="radio" name="optionsRadios" value="1" >Platform
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input class="check-radio" type="radio" name="optionsRadios"  value="2">Shopify
                                                </label>
                                            </div>
                                        </div></td>
                                     <td class="hidden post-feed"><?=json_encode($p['feed_ads'])?></td>
                                    <td>
                                    	<button class="btn btn-success btn-sm btn-action" data-action="1"><i class="fa fa-check"></i> Accept</button>
                                        <button class="btn btn-warning btn-sm btn-action" data-action="2"><i class="fa  fa-times"></i> Denied</button>
                                        <button class="btn btn-danger btn-sm btn-action" data-action="3"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                   </div>                 
                </div>
 </div>
<div class="list-post">
    <table class="table-striped table-hover">
    	<tbody class="list-feed">
            <tr class="list-row">
                <td class="list-img"><img src=""  width="130" height="150"/></td>
                <td class="list-txt"></td>
            </tr>
       </tbody>
    </table>
</div>
<script>
	$(document).ready(function(){
			var parent,btnAction,typePage,pageId;
			$('.btn-detail').click(function(e){
				e.stopPropagation();
			var html 	 = '',postFeed;
				parent 	 = $(this).parents('tr');
				postFeed = parent.find('.post-feed').text();
				postFeed = JSON.parse(postFeed);
				$.each(postFeed,function(k,v){
						html += '<tr>';
							html += '<td class="list-img"><img src="'+v.img+'"  width="130" height="150"/>';
							html += '</td>';
							html += ' <td class="list-txt">';
							
							html += v.content;
							html += '</td>';
						html += '</tr>';
					});
				$('.list-feed').html(html);
				$('.list-post').show();
				$('.list-post').scrollTop(0);
			});
			$('.main-page').click(function(){
				$('.list-post').hide();
			});
			$('.btn-action').click(function(){
				parent	  = $(this).parents('tr');
				btnAction = $(this).attr('data-action');
				var id	  = parent.attr('data-id');
				if(btnAction == 1)
				{
					typePage = parent.find('.check-radio:checked').val();
					if(!typePage)
					{
						alert('Plz Check Type Product');
						return;
					}
				}
				$.ajax({
					url:'adssuccess/ajax/action_page',
					type:'post',
					data:{'action':btnAction,'id':id,'product':typePage},
					beforeSend: function()
					{
						parent.find('.btn-action').prop('disabled',true);
					},
					success: function(res){
						parent.find('.btn-action').prop('disabled',false);
						res = res.trim();
						if(res == 'active')
							parent.css('background','#5cb85c');
						else if(res == 'hide')
							parent.css('background','#f0ad4e');
						else
							parent.css('background','#d9534f');
					}
					});
						
			});
			function linkify(inputText)
			{
				var replacedText, replacePattern1, replacePattern2, replacePattern3;
			
				//URLs starting with http://, https://, or ftp://
				replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
				replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
			
				//URLs starting with "www." (without // before it, or it'd re-link the ones done above).
				replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
				replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
			
				//Change email addresses to mailto:: links.
				replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
				replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');
			
				return replacedText;
			}
		});
</script>
<style>
	.list-post{
		position:fixed;
		height:100%;
		overflow:scroll;
		left:0;
		top:0;
		width:500px;
		border-right:solid 1px #ccc;
		background:white;
		z-index:999999;
		display:none;
	}
	.list-row{
		
		}
	.list-img{
		padding:6px 0px 6px 10px;
		width:40%;
	}
	.list-txt{
		padding-left:10px;
		}
</style>