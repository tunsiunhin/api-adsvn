<style>
.acc-filter{
	padding-bottom:20px;
	padding-top:15px
}
.acc-filter>div{
		float:left;
		margin-right:10px;	
}

.acc-add-p{
	float:right !important;	
}
.account-search{
	width:250px	
}
.input-account-search{
	position:relative;	
}
.input-account-search i{
	position:absolute;
	right:10px;
	top:10px;
	color:#ccc	
}
.table-acc td{
	vertical-align:middle !important	
}
.table-acc td:last-child>div{
	display:inline-block;
	margin-left:5px;	
}
.btn-pause-acc{
	width:66px	
}

.pagi{
	display:inline-block
}
.pagi strong, .pagi a{
	padding:10px;	
}


</style>



<div class="clearfix acc-filter">
    <div class="input-account-search">
        
        <div class="dropdown">
            <i class="fa fa-search"></i>
            <input class="form-control account-search dropdown-toggle" data-toggle="dropdown" placeholder="Search account" type="search">
            
            <div class="dropdown-menu">
                <div style="padding:5px 10px">
                Press Enter to search
                </div>
            </div>
        </div>
    </div>
    
    <div>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">All account
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="" class="account-status">All account</a></li>
            <li><a href="1" class="account-status">Active account</a></li>
            <li><a href="4" class="account-status">Checkpoint account</a></li>
            <li><a href="5" class="account-status">Limit account</a></li>
            <li><a href="0" class="account-status">Error account</a></li>
          </ul>
        </div>
    </div>
    
    <div>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort by
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="" class="account-sort">All account</a></li>
            <li><a href="1" class="account-sort">Active account</a></li>
            <li><a href="4" class="account-sort">Checkpoint account</a></li>
            <li><a href="5" class="account-sort">Limit account</a></li>
            <li><a href="0" class="account-sort">Error account</a></li>
          </ul>
        </div>
    </div>
    
    
   <div class="acc-add-p">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_profile">Add new account</button>
    </div>
    
    
</div>

<style>
	.alert-info span{
		margin-right:15px	
	}
</style>

<div class="alert alert-info" role="alert" style="margin-bottom:15px">
	<span>Total <strong><?=$total_account?></strong></span>
    <span>Error <strong><?=$total_error?></strong></span>
    <span>Checkpoint <strong><?=$total_checkpoint?></strong></span>
    <span>Limit <strong><?=$total_limit?></strong></span>
</div>

<div>
        <table class="table table-striped table-acc">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Proxy</th>
                    <th>Page count</th>
                    <th>Scan type</th>
                    <th>Next run</th>
                    <th>Status</th>
                    <th style="width:140px">Action</th>
                </tr>
            </thead>
            <tbody class="tb-ajax">
            
           
            </tbody>
        </table>
</div>

<div class="nodata text-center" style="display:none;padding:5px">
	<h5>No data available!</h5>
</div>

<div class="loading text-center" style="padding:20px 0;">
	<div style="display:;font-size:24px"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
</div>

<div class="text-center">
    <div class="pagi">
        
    </div>
</div>



<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
       <div class="modal-dialog" role="document">
          
          <div class="modal-content">
             
             <div class="modal-header">
                <h4 class="modal-title"><strong>ADD NEW ACCOUNT</strong></h4>
             </div>
             
             <div class="modal-body"> 
             	
                <div class="form-group">
            		<h5><strong>List account</strong></h5>
                	<textarea rows="7" class="form-control list-account"></textarea>
            	</div>
                 <div class="form-group">
            		<h5><strong>Proxy</strong></h5>
                	<select class="form-control fbproxy">
                    	<option selected="selected" value="">Choose a proxy</option>
                        <?php foreach($proxys as $px) { ?>
                        <option value="<?=$px['id']?>"><?=$px['name'].' => '.$px['host']?></option>
                        <?php } ?>
                    </select>
            	</div>
             </div>
             
             <div class="modal-footer text-right">
                   <button type="button" class="btn btn-primary btn-add-account">Add account</button>
             </div>
             
          </div>
          
       </div>
</div> 

<div class="modal fade" id="check_acc" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content" style="padding:20px;">
    		<div class="check-acc-loading" style="display:;font-size:24px;text-align:center"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
            <div class="content-ajax" style="overflow:auto;max-width:100%;max-height:500px">
            
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="show_acc" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
    	<div class="modal-content" style="padding:0 5px;">
    		
             <div class="modal-body"> 
             	<input class="form-control" type="hidden" name="acc_id" />
             	<div class="form-group">
            		<h5><strong>Cookie</strong></h5>
                	<textarea rows="7" class="form-control" name="cookie"></textarea>
            	</div>
                <div class="form-group">
            		<h5><strong>Proxy</strong></h5>
                	<input class="form-control" name="proxy" />
            	</div>
                <div class="form-group">
            		<h5><strong>Fbid</strong></h5>
                	<input class="form-control" name="fbid" />
            	</div>
                <div class="form-group">
            		<h5><strong>Password</strong></h5>
                	<input class="form-control" name="password" />
            	</div>
                <div class="form-group">
            		<h5><strong>User agent</strong></h5>
                	<input class="form-control" name="useragent" />
            	</div>
                
                <div>
                	<button class="btn btn-success btn-update-acc">Update</button>
                </div>
            </div>
            
        </div>
    </div>
</div>
      




<script>
$(document).ready(function(e) {

var rq;
var param = {};



load_ajax();

function load_ajax()
{
	if (rq != null) { 
		rq.abort();
		rq = null;
	}
	
	
	$('.nodata').hide();
	$('.tb-ajax').hide();
	$('.loading').show();
		
	rq = $.ajax({
		url:'adssuccess/fbaccount/load',
		data:param,
		dataType:"json",
		success: function(res) {
			$('.loading').hide();
			if(res.html)
				$('.tb-ajax').html(res.html).show();
			else
				$('.nodata').show();
				
			$('.pagi').html(res.pagination);
		}
	});	
}

$('.account-search').keyup(function(e) {
    if(e.which == 13) {
		var q = $(this).val();
		if(q) {
			param['q'] = q;
			param['page'] = 0;
			load_ajax();	
		}	
	}
});

$('.account-status').bind('click',function(e){
	e.preventDefault();
	
	var status = $(this).attr('href');
	
	param['status'] = status;
	param['page'] = 0;
	
	load_ajax();	
});

$(this).on('click','.pagi a',function(e) {
	
	var page = $(this).attr('data-page');
	
	param['page'] = page;
	
	load_ajax();
});




$('.btn-add-account').click(function(e) 
{
    var account = $('.list-account').val();
	var proxy_id = $('.fbproxy').find('option:selected').val();
	
	$this = $(this);
	$this.prop('disabled',true);
	
	$.ajax({
		url:'adssuccess/fbaccount/add',
		data:{'account':account,'proxy_id':proxy_id},
		type:'POST',
		dataType:"json",
		success: function(res) 
		{
			$this.prop('disabled',false);
			
			if(res.code == 200 ){
				location.reload();	
			}			
			else
				alert(res.message);
		}
	});
});

$(this).on('click','.btn-show-acc',function()
{
	var $this = $(this);
	
	var acc_id = $this.parents('tr').attr('data-id');
	var cookie = $this.parents('tr').find('.acc-cookie').text();
	var proxy = $this.parents('tr').find('.acc-proxy').text();
	var password = $this.parents('tr').find('.acc-password').text();
	var fbid = $this.parents('tr').find('.acc-fbid').text();
	var useragent = $this.parents('tr').find('.acc-useragent').text();
	
	$('#show_acc').find('[name=acc_id]').val(acc_id);
	$('#show_acc').find('[name=cookie]').val(cookie);
	$('#show_acc').find('[name=proxy]').val(proxy);
	$('#show_acc').find('[name=fbid]').val(fbid);
	$('#show_acc').find('[name=password]').val(password);
	$('#show_acc').find('[name=useragent]').val(useragent);
	
	$('#show_acc').modal();
	//$(this).find('input.acc-cookie').select();
	//document.execCommand("copy");
		
});

$(this).on('click','.btn-pause-acc',function() {
	
	var acc_id = $(this).parents('tr').attr('data-id');
	
	$this = $(this);
	$this.prop('disabled',true);
	
	$.ajax({
		url:'adssuccess/fbaccount/pause',
		data:{'acc_id':acc_id},
		type:'POST',
		dataType:'json',
		success: function(res) 
		{
			$this.prop('disabled',false);
			if(res.code == 200) {
				if(res.message == 1){
					$this.find('span').text('Start');
					$this.parents('tr').find('.acc-status').append('<br /><span class="label label-warning">Stopped</span>');
				}
				else{
					$this.find('span').text('Pause');
					$this.parents('tr').find('.acc-status .label-warning').remove();
					$this.parents('tr').find('.acc-status br').remove();
					
				}
			}
			else
				alert(res.message);
			
		}
	});		
});


$('.btn-update-acc').click(function(e)
{
	
	var param = {
		'acc_id':$('#show_acc').find('[name=acc_id]').val(),
		'cookie':$('#show_acc').find('[name=cookie]').val(),
		'password':$('#show_acc').find('[name=password]').val()
	}
	
	$this = $(this);
	
	$.ajax({
		url:'adssuccess/fbaccount/update',
		data:param,
		type:'POST',
		dataType:'json',
		success: function(res) 
		{
			alert(res.message);
		}
	});	
});	

$(this).on('click','.btn-check-acc',function(e)
{
	
	e.stopPropagation();
	
	var acc_id = $(this).parents('tr').attr('data-id');
	
	$this = $(this);
	if($this.text() == 'Checking...') {
		return false;	
	}
	$this.text('Checking...');
	
	var rq = $.ajax({
		url:'adssuccess/fbaccount/check',
		data:{'acc_id':acc_id},
		type:'POST',
		
		success: function(res) 
		{
			$this.text('Check live');
			alert(res.message);
		}
	});	
});



$(this).on('click','.btn-language',function()
{
	
	var acc_id = $(this).parents('tr').attr('data-id');
	
	$this = $(this);
	$this.prop('disabled',true);
	
	$.ajax({
		url:'adssuccess/fbaccount/language',
		data:{'acc_id':acc_id},
		type:'POST',
		dataType:"json",
		success: function(res) 
		{
			$this.prop('disabled',false);
			//alert(res.message);
		}
	});	
});	
});
</script>