 <?php  function fm_num($num){
		if($num > 1000 && $num < 1000000)
			return round($num/1000,1).'K';
		else if($num >= 1000000)
			return round($num/1000000,1).'M';
		else
			return $num;
 } ?>
<style>
.page-filter{
	padding-top:15px;	
	padding-bottom:10px;
	
}
.page-filter>div{
	float:left	
}
.btn-niche{
    position:relative;  
    display:inline-block;
}
.btn-niche a{
    position:absolute;
    width:100%;
    height:100%;
    z-index:100;
    opacity:0.5;
    top:0;
    left:0;
    background:#000;
    display:none;
}
.btn-niche button{
    z-index:1;
    width:100%;
        
}
</style>
<script type="text/javascript">
    $(document).ready(function(e) 
{   
    $('.show-niche').bind('click',function(){
        
        var pos = $(this).offset();
        
        $('.list-niche').css({'top':pos.top,'left':pos.left});

        $('.list-niche').show();
        
        $('.selectpicker').selectpicker('deselectAll'); 

        $('.selectpicker').selectpicker('toggle');
        
    });
    
});
</script>
<!-- <div class="clearfix page-filter">
	<div>
    	Hello <strong><?=$admin['name']?></strong>
    </div>
    <div style="float:right">
    	<span style="margin-right:15px">Total <strong><?=number_format($admin['approved_total'])?></strong></span>
    	<span style="margin-right:15px">Today <strong><?=number_format($admin['approved_day'])?></strong></span>
        <span>This month <strong><?=number_format($admin['approved_month'])?></strong></span>
    </div>
</div> -->	

<div class="list-niche" style="width: 110px;position:absolute;display: none;">
    <select class="selectpicker" multiple data-size="10" data-width="100%" data-live-search="true" title="Niche">
        
      <?php 
        foreach($niches as $row) 
        {
            if($row['category_id'] == 0){
                echo '<optgroup label="'.strtoupper($row['name']).'">';
                foreach($niches as $k => $r) 
                {
                    if($r['category_id'] == $row['id'])
                        echo'<option value="'.$r['id'].'">'.ucfirst($r['name']).'</option>';
                }
                echo '</optgroup>';
                    
                //unset($niches[$k]);
            }
                
        }
      ?>
    </select>
</div>
<div class="alert alert-warning" role="alert" style="margin-bottom:5px">
	Cố gắng xét kỹ trước khi denined. Có để lại rồi hỏi admin trên facebook
</div>

<div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th style="width: 10%">#</th>
                <th style="width: 15%">Page Name</th>
                <th>Post</th>
                <th style="width: 10%">Select</th>
                <th style="width:10%">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($pages as $p){?>
                <tr data-id="<?=$p['id_page']?>">
                <td> <a target="_blank" href="<?=$p['page_url']?>" ><img class="img-circle" width="50" height="50" src="<?=$p['page_picture']?>" /></a></td>
                <td class="align-right"><?=$p['page_name']?></td>
                <td>
                	<div class="row">
                	<?php
                	foreach ($p['list_post'] as $key => $value) {?>
            			<div style="overflow: hidden;max-height: 200px" class="col-md-4">
            				<div>
            					<img width="100%" src="<?= $value['picture']?>">
            				</div>
            				<div>
            					<p><?= $value['content']?></p>
            				</div>
            			</div>
                	<?php } ?>
                	</div>
                </td>
                <td>
                	<div class="btn-niche">
                        <button class="btn btn-default show-niche">choose niche</button>
                    </div>
                </td>
                <td class="hidden post-feed"><?=$p['feed_ads']['html']?></td>
                <td>
                    <button class="btn btn-success btn-sm btn-action" data-action="1"><i class="fa fa-check"></i> Accept</button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div class="list-post">
    
</div>

<style>
	.list-post{
		position:fixed;
		height:100%;
		overflow:scroll;
		left:0;
		top:0;
		width:500px;
		border-right:solid 1px #ccc;
		background:white;
		z-index:999999;
		display:none;
	}
	.list-post ._643h{
		border-bottom:1px solid #ddd;
		margin-bottom:30px;	
		padding-bottom:30px;
	}
	.list-post ._643h>div:first-child{
		display:none;
	}
	.list-post .d_1q1namiuwc, .list-post ._x8e,.list-post ._34k3,.list-post ._43f9._63qh, .list-post ._5mly._40ja._45oh, .list-post ._5pcp._5lel._2jyu._232_, .list-post ._38vo, .list-post .fwn.fcg{
		display:none;	
	}
	.list-post .uiScaledImageContainer{
		height:auto !important;
	}	
	.list-post img{
		max-width:200px;
		height:auto !important;
		
	}
	
</style>

<script>
$(document).ready(function(e) {
	$('.btn-action').on('click', function(){
			//var list_niche = $(this).parents('tr').find('select').val();
            var list_niche = $('.list-niche').find('select').val();
			var page_id = $(this).parents('tr').attr('data-id');
			var _this = $(this);
            //console.log(list_niche);
//            return;
			$.ajax({
				url: 'adssuccess/page/approve_niche',
				type: 'post',
				data: {'list_niche': list_niche, 'page_id': page_id},
				success: function(res){
					if(res == 1){
						_this.parents('tr').css('background','#81e681');
					}else{
						alert(res);
					}
					
				}
			});
		});
});
</script>