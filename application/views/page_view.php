<style>
.page-filter>div{
	float:left;	
}
.page-filter .statistic span{
	margin-left:15px;
}

.acc-filter{
	padding-bottom:20px;
	padding-top:15px;
	border-bottom:1px solid #eee;
}
.acc-filter>div{
		float:left;
		margin-right:10px;	
}

.acc-add-p{
	float:right !important;	
}
.account-search{
	width:250px	
}
.input-account-search{
	position:relative;	
}
.input-account-search i{
	position:absolute;
	right:10px;
	top:10px;
	color:#ccc	
}
.table-acc td{
	vertical-align:middle !important	
}
.table-acc td:last-child>div{
	display:inline-block;
	margin-left:5px;	
}

.pagi{
	display:inline-block
}
.pagi strong, .pagi a{
	padding:10px;	
}
</style>

<ol class="breadcrumb">
  <li><a href="/">Dashboard</a></li>
  <li class="active">Page manager</li>
</ol>

<div role="alert" class="alert alert-success clearfix page-filter" style="margin-bottom:5px">
	<div class="statistic" style="float:right">
    	<span>Total <strong><?=number_format($total)?></strong></span>
        <span>Scanned <strong><?=number_format($total_scan)?></strong></span>
        <span>Approved <strong><?=number_format($total_approved)?></strong></span>
        <span>Denined <strong><?=number_format($total_denined)?></strong></span>
        <span>Need approve <strong><?=number_format($total_need_approved)?></strong></span>
        <span>Error <strong><?=number_format($total_error)?></strong></span>
    </div>	
</div>

<div class="clearfix acc-filter">
    <div class="input-account-search">
        
        <div class="dropdown">
            <i class="fa fa-search"></i>
            <input class="form-control account-search dropdown-toggle" data-toggle="dropdown" placeholder="Search page" type="search">
            
            <div class="dropdown-menu">
                <div style="padding:5px 10px">
                Press Enter to search
                </div>
            </div>
        </div>
    </div>
    
    <div>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">All account
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="" class="account-status">All page</a></li>
            <li><a href="2" class="account-status">Approved page</a></li>
            <li><a href="3" class="account-status">Denied page</a></li>
            <li><a href="0" class="account-status">No Data</a></li>
            <li><a href="0" class="account-status">Error page</a></li>
          </ul>
        </div>
    </div>
    
    <div>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort by
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="" class="account-sort">All account</a></li>
            <li><a href="scan_count" data-val="1" class="page-sort">Scan count A-Z</a></li>
            <li><a href="scan_count" data-val="-1" class="page-sort">Scan count Z-A</a></li>
          </ul>
        </div>
    </div>
    
    <div>
    	<span style="line-height:36px">Results <strong class="ajax-total"></strong></span>
    </div>
    
    
   <div class="acc-add-p">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_profile">Add new page</button>
    </div>
    
    
</div>


<div>
    <table class="table table-striped table-page">
        <thead>
            <tr>
                <th>Picture</th>
                <th>Name</th>
                <th>Scan count</th>
                <th>Like</th>
                <th>Type</th>
                <th>Status</th>
                <th style="width:140px">Action</th>
            </tr>
        </thead>
        <tbody class="tb-ajax">
        
       
        </tbody>
    </table>
</div>

<div class="nodata text-center" style="display:none;padding:5px">
	<h5>No data available!</h5>
</div>

<div class="loading text-center" style="padding:20px 0;">
	<div style="display:;font-size:24px"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
</div>

<div class="text-center">
    <div class="pagi">
        
    </div>
</div>

<script>
$(document).ready(function(e) {

	var rq;
	var param = {};
		
	load_ajax();
	
	function load_ajax()
	{
		if (rq != null) { 
			rq.abort();
			rq = null;
		}
		
		
		$('.nodata').hide();
		$('.tb-ajax').hide();
		$('.loading').show();
			
		rq = $.ajax({
			url:'adssuccess/page/load',
			data:param,
			dataType:"json",
			success: function(res) {
				$('.loading').hide();
				if(res.html)
					$('.tb-ajax').html(res.html).show();
				else
					$('.nodata').show();
					
				$('.pagi').html(res.pagination);
				$('.ajax-total').text(res.total);
			}
		});	
	}
	
	$('.account-search').keyup(function(e) {
		if(e.which == 13) {
			var q = $(this).val();
			if(q) {
				param['q'] = q;
				param['page'] = 0;
				load_ajax();	
			}	
		}
	});
	
	$('.account-status').bind('click',function(e){
		e.preventDefault();
		
		var status = $(this).attr('href');
		
		param['status'] = status;
		param['page'] = 0;
		
		load_ajax();	
	});
	
	$('.page-sort').bind('click',function(e){
		e.preventDefault();	
		
		var sortby = $(this).attr('href');
		var sortval = $(this).attr('data-val');
		
		param['sortby'] = status;
		param['sortval'] = sortval;
		param['page'] = 0;
		
		load_ajax();	
	});
	
	$(this).on('click','.pagi a',function(e) {
		
		var page = $(this).attr('data-page');
		
		param['page'] = page;
		
		load_ajax();
	});
	
	$(this).on('click','.btn-delete-page',function(e)
	{
		var page_id = $(this).parents('tr').attr('data-id');	
		
		$this = $(this);
		
		$this.prop('disabled',true);
				
		var rq = $.ajax({
			url:'adssuccess/page/hide',
			data:{'page_id':page_id},
			type:'POST',
			dataType:'json',
			success: function(res) 
			{
				$this.prop('disabled',false);	
			}
		});	
	});

});
</script>