<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'adssuccess/main/dashboard';
$route['test'] = 'adssuccess/main/dashboard';
$route['404_override'] 		 = '';
$route['login']				 = 'adssuccess/member/login';
$route['logout']			 = 'adssuccess/member/logout';
$route['check-login']		 = "adssuccess/member/process_login";

$route['robot']				 = 'adssuccess/main/robot';
$route['analytics']			 = 'adssuccess/main/analytic';
$route['approval-page']		 = 'adssuccess/main/fbposts_page';
$route['approval']			 = 'adssuccess/main/fbposts';
$route['ajax/(:any)']		 = 'adssuccess/ajax/index/$1';
$route['cronjob/scan-img']	 = 'adssuccess/cronjob/scan'; // scan img accept page
$route['cronjob/get-token']  = 'adssuccess/cronjob/get_token'; // get token ads for extension
$route['check']              = 'adssuccess/check';
$route['browser']            = 'adssuccess/browser';

$route['approve/page']	 	 = 'adssuccess/view/approve_page';
$route['approve/post']	 	 = 'adssuccess/view/approve_post';
$route['member']            = 'adssuccess/view/member';
$route['account']            = 'adssuccess/view/account';
$route['proxy']              = 'adssuccess/view/proxy';
$route['page']               = 'adssuccess/view/page';
$route['post']               = 'adssuccess/view/post';
$route['keyword']            = 'adssuccess/view/keyword';
$route['niche']              = 'adssuccess/view/niche';
$route['categories']		 = 'adssuccess/view/categories';


/* Quản trị api */

$route['crawl/pId2pTmp']	= 'crawl/ids2tmps/index';
$route['api/pushIds']		= 'crawl/pushIds/index';
$route['crawl/pId2pages']	= 'crawl/ids2tmps/idsToPages';
$route['crawl/tPages2page'] = 'crawl/tPages2Pages/index';
$route['pictureAmz']		= 'crawl/uploadAmz/execute';
$route['api/getUrl']		= 'crawl/pushIds/getUrl';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
